<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_en');
            $table->string('slug');
            $table->string('images');
            $table->boolean('active');
            $table->integer('orderBy');
            $table->text('description');
            $table->text('parent');
            $table->timestamps();
        });
        $product_categories = array(
  array('id' => '1','name' => 'Đồ chơi cho bé trai','name_en' => 'Đồ chơi trẻ em','slug' => 'do-choi-cho-be-trai','images' => '062427_dochoivabe_full_27402015_124052.jpg','active' => '1','orderBy' => '1','description' => '<p>Đồ chơi trẻ em</p>

<p>\\r\\n</p>
','parent' => '0','created_at' => '2016-10-19 06:24:50','updated_at' => '2016-10-19 06:24:50'),
  array('id' => '2','name' => 'Đồ chơi cho bé gái','name_en' => 'Đồ chơi học tập','slug' => 'do-choi-cho-be-gai','images' => '064139_cm_b32809.jpg','active' => '1','orderBy' => '2','description' => '','parent' => '0','created_at' => '2016-10-19 06:41:57','updated_at' => '2016-10-19 06:41:57'),
  array('id' => '3','name' => 'Đồ chơi cho bé sơ sinh','name_en' => '','slug' => 'do-choi-cho-be-so-sinh','images' => '065005_top_10_do_choi_an_toan_cho_be_so_sinh_2.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:50:27','updated_at' => '2016-10-19 06:50:27'),
  array('id' => '4','name' => 'Đồ chơi lắp ráp sáng tạo','name_en' => '','slug' => 'do-choi-lap-rap-sang-tao','images' => '062803_xediahinh_1.jpg','active' => '1','orderBy' => '0','description' => '<p>dsaf</p>
','parent' => '1','created_at' => '2016-10-19 06:28:37','updated_at' => '2016-10-19 06:28:37'),
  array('id' => '5','name' => 'xe cho bé đủ loại','name_en' => '','slug' => 'xe-cho-be-du-loai','images' => '','active' => '1','orderBy' => '5','description' => '','parent' => '5','created_at' => '2016-10-06 03:30:41','updated_at' => '2016-10-06 03:30:41'),
  array('id' => '6','name' => 'Thú nhồi bông','name_en' => '','slug' => 'thu-nhoi-bong','images' => '065343_758044ad10d8bb00990385c3b981a0a5.jpg','active' => '1','orderBy' => '1','description' => '','parent' => '0','created_at' => '2016-10-19 06:53:57','updated_at' => '2016-10-19 06:53:57'),
  array('id' => '7','name' => 'Đồ dùng sơ sinh','name_en' => '','slug' => 'do-dung-so-sinh','images' => '065422_maxresdefault_2.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:54:38','updated_at' => '2016-10-19 06:54:38'),
  array('id' => '8','name' => 'Tã - Bĩm','name_en' => '','slug' => 'ta-bim','images' => '065529_17.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:55:44','updated_at' => '2016-10-19 06:55:44'),
  array('id' => '9','name' => 'Bình sữa - Máy hút sữa','name_en' => '','slug' => 'binh-sua-may-hut-sua','images' => '065621_hut_sua_bang_tay_lasinoh_1_copyright_yeucon247com_chuyen_binh_sua_do_choi_do_dung_thuc_pham_ngoai_nhap_cao_cap_danh_cho_me_va_be.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:56:40','updated_at' => '2016-10-19 06:56:40'),
  array('id' => '10','name' => 'Gấu bông ','name_en' => '','slug' => 'gau-bong','images' => '065343_758044ad10d8bb00990385c3b981a0a5.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '6','created_at' => '2016-10-19 07:51:42','updated_at' => '2016-10-19 07:51:42'),
  array('id' => '15','name' => 'Nệm mùng cho  bé','name_en' => '','slug' => 'nem-mung-cho-be','images' => '075613_e247221e03b74c1affd1e9545b1cd7c5.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '7','created_at' => '2016-10-19 07:56:51','updated_at' => '2016-10-19 07:56:51'),
  array('id' => '16','name' => 'Bình sữa Cacara ','name_en' => '','slug' => 'binh-sua-cacara','images' => '075827_extendimage_1658_d3.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '7','created_at' => '2016-10-19 07:58:59','updated_at' => '2016-10-19 07:58:59'),
  array('id' => '17','name' => 'Tã cho bé','name_en' => '','slug' => 'ta-cho-be','images' => '065529_17.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '8','created_at' => '2016-10-19 08:02:02','updated_at' => '2016-10-19 08:02:02'),
  array('id' => '18','name' => 'Bĩm cho bé','name_en' => '','slug' => 'bim-cho-be','images' => '080115_huong_dan_cach_chon_bim_ta_lot_tot_cho_tre_so_sinh_cho_cac_ba_me_4.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '8','created_at' => '2016-10-19 08:01:52','updated_at' => '2016-10-19 08:01:52'),
  array('id' => '19','name' => 'Bình sữa','name_en' => '','slug' => 'binh-sua','images' => '080239_bo_2_binh_sua_comotomo_250ml_1448426587.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '9','created_at' => '2016-10-19 08:02:55','updated_at' => '2016-10-19 08:02:55'),
  array('id' => '20','name' => 'Máy hút sữa','name_en' => '','slug' => 'may-hut-sua','images' => '065621_hut_sua_bang_tay_lasinoh_1_copyright_yeucon247com_chuyen_binh_sua_do_choi_do_dung_thuc_pham_ngoai_nhap_cao_cap_danh_cho_me_va_be.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '9','created_at' => '2016-10-19 08:03:39','updated_at' => '2016-10-19 08:03:39'),
  array('id' => '24','name' => 'Đồ chơi gỗ thông minh','name_en' => '','slug' => 'do-choi-go-thong-minh','images' => '062918_do_choi_go.jpg','active' => '1','orderBy' => '0','description' => '<p>dfsa</p>
','parent' => '1','created_at' => '2016-10-19 06:29:39','updated_at' => NULL),
  array('id' => '25','name' => 'Đồ chơi điều khiển','name_en' => '','slug' => 'do-choi-dieu-khien','images' => '063008_medium_large_02_2012_8ecebf16c698d7c059a95269f24b90fagif.jpg','active' => '1','orderBy' => '0','description' => '<p>tfyt</p>
','parent' => '1','created_at' => '2016-10-19 06:30:26','updated_at' => NULL),
  array('id' => '26','name' => 'Siêu nhân - Robot','name_en' => '','slug' => 'sieu-nhan-robot','images' => '063058_maxresdefault.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '1','created_at' => '2016-10-19 06:31:15','updated_at' => NULL),
  array('id' => '27','name' => 'Đồ chơi động vật','name_en' => '','slug' => 'do-choi-dong-vat','images' => '063729_hqdefault.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '1','created_at' => '2016-10-19 06:37:55','updated_at' => NULL),
  array('id' => '28','name' => 'Mô hình phương tiện giao thông','name_en' => '','slug' => 'mo-hinh-phuong-tien-giao-thong','images' => '063848_choi_lego3.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '1','created_at' => '2016-10-19 06:39:12','updated_at' => NULL),
  array('id' => '29','name' => 'Đồ chơi nhạc cụ','name_en' => '','slug' => 'do-choi-nhac-cu','images' => '064013_dfn1387957484.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '1','created_at' => '2016-10-19 06:41:02','updated_at' => NULL),
  array('id' => '30','name' => 'Đồ chơi búp bê','name_en' => '','slug' => 'do-choi-bup-be','images' => '064222_ban_do_choi_bup_be_barbie_gia_re_bo_suu_tap_thoi_trang_nganh_nghe_430198j19722x300x300.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:42:38','updated_at' => NULL),
  array('id' => '31','name' => 'Đồ chơi nhà bếp','name_en' => '','slug' => 'do-choi-nha-bep','images' => '064307_maxresdefault_1.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:43:25','updated_at' => NULL),
  array('id' => '32','name' => 'Đồ chơi giả vai','name_en' => '','slug' => 'do-choi-gia-vai','images' => '064406_30pcs_kids_pretend_role_play_font_b_supermarket_b_font_font_b_set_b_font_basket.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:44:29','updated_at' => NULL),
  array('id' => '33','name' => 'Khéo tay hay làm','name_en' => '','slug' => 'kheo-tay-hay-lam','images' => '064517_hoa_dep.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:45:31','updated_at' => NULL),
  array('id' => '34','name' => 'Đồ chơi nhạc cụ','name_en' => '','slug' => 'do-choi-nhac-cu','images' => '064606_248809759bo_nhac_cu16.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:46:22','updated_at' => NULL),
  array('id' => '35','name' => 'Lắp ráp sáng tạo','name_en' => '','slug' => 'lap-rap-sang-tao','images' => '064749_bo_lap_rap_sang_tao_winwintoys_64302_xe.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 07:25:15','updated_at' => '2016-10-19 07:25:15'),
  array('id' => '36','name' => 'Đồ chơi gỗ thông minh','name_en' => '','slug' => 'do-choi-go-thong-minh','images' => '064854_ngang21388152515.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '2','created_at' => '2016-10-19 06:49:13','updated_at' => NULL),
  array('id' => '37','name' => 'Nhóm <6 tháng tuổi','name_en' => '','slug' => 'nhom-6-thang-tuoi','images' => '065110_lua_qua_16_cho_be_theo_do_tuoi_1.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '3','created_at' => '2016-10-19 06:52:02','updated_at' => NULL),
  array('id' => '38','name' => 'Nhóm >6 tháng tuổi','name_en' => '','slug' => 'nhom-6-thang-tuoi','images' => '065245_thu_go_mon_do_choi_go_an_toan_va_huu_ich_cho_be_ma_cac_ba_me_nen_quan_tam.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '3','created_at' => '2016-10-19 06:53:01','updated_at' => NULL),
  array('id' => '39','name' => 'Ghế ăn - Nôi cũi  - Xe đẩy','name_en' => '','slug' => 'ghe-an-noi-cui-xe-day','images' => '065724_20131112101052.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:57:53','updated_at' => NULL),
  array('id' => '40','name' => 'Xe tập đi','name_en' => '','slug' => 'xe-tap-di','images' => '065817_xe_tap_di_bang_go.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:58:29','updated_at' => NULL),
  array('id' => '41','name' => 'Xe lắc - Xe chòi chân','name_en' => '','slug' => 'xe-lac-xe-choi-chan','images' => '065858_lcj1413455781.JPG','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 06:59:15','updated_at' => NULL),
  array('id' => '42','name' => 'Xe 3 bánh - Xe đạp','name_en' => '','slug' => 'xe-3-banh-xe-dap','images' => '070001_xedap.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 07:00:15','updated_at' => NULL),
  array('id' => '43','name' => 'Xe moto - Ô tô điện','name_en' => '','slug' => 'xe-moto-o-to-dien','images' => '070055_xe_o_to_dien_tre_em_police_max_1118_1.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '0','created_at' => '2016-10-19 07:01:15','updated_at' => NULL),
  array('id' => '44','name' => 'Pokemon','name_en' => '','slug' => 'pokemon','images' => '075346_offre_speciale_pikachu_jouets_en_peluche_tres_mignon_pokemon_jouets_en_peluche_pokemon_douce_peluche_jouets.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '6','created_at' => '2016-10-19 07:54:03','updated_at' => NULL),
  array('id' => '45','name' => 'Ghế ăn cho bé','name_en' => '','slug' => 'ghe-an-cho-be','images' => '080434_146_ghe_an_em_be_kora_co_nem.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '39','created_at' => '2016-10-19 08:04:50','updated_at' => NULL),
  array('id' => '46','name' => 'Nôi cũi cho bé','name_en' => '','slug' => 'noi-cui-cho-be','images' => '080520_210_combo_cui_xuat_khao_70.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '39','created_at' => '2016-10-19 08:05:38','updated_at' => NULL),
  array('id' => '47','name' => 'Xe đẩy cho bé','name_en' => '','slug' => 'xe-day-cho-be','images' => '080616_xe_day_em_be.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '39','created_at' => '2016-10-19 08:06:38','updated_at' => NULL),
  array('id' => '48','name' => 'Xe 3 bánh','name_en' => '','slug' => 'xe-3-banh','images' => '080733_song_long_f1_3_logo.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '42','created_at' => '2016-10-19 08:07:49','updated_at' => NULL),
  array('id' => '49','name' => 'xe đạp cho bé','name_en' => '','slug' => 'xe-dap-cho-be','images' => '080831_xe_dap_cho_be_gai_mau_hong_totem_1139_750x750.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '42','created_at' => '2016-10-19 08:08:53','updated_at' => NULL),
  array('id' => '50','name' => 'xe lắc','name_en' => '','slug' => 'xe-lac','images' => '065858_lcj1413455781.JPG','active' => '1','orderBy' => '0','description' => '','parent' => '41','created_at' => '2016-10-19 08:09:31','updated_at' => NULL),
  array('id' => '51','name' => 'xe chòi chân cho bé','name_en' => '','slug' => 'xe-choi-chan-cho-be','images' => '081025_xe_choi_chan_o_to_grand_coupe_little_tikes_lt_445830091_cho_be.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '41','created_at' => '2016-10-19 08:10:41','updated_at' => NULL),
  array('id' => '52','name' => 'Xe moto điện','name_en' => '','slug' => 'xe-moto-dien','images' => '081112_xe_may_dien_phan_khoi_lon.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '43','created_at' => '2016-10-19 08:11:34','updated_at' => NULL),
  array('id' => '53','name' => 'Xe oto điện ','name_en' => '','slug' => 'xe-oto-dien','images' => '070055_xe_o_to_dien_tre_em_police_max_1118_1.jpg','active' => '1','orderBy' => '0','description' => '','parent' => '43','created_at' => '2016-10-19 08:12:20','updated_at' => NULL)
);

      \Illuminate\Support\Facades\DB::table('product_categories')->insert($product_categories);}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('product_categories');
    }

}
