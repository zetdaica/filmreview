<?php

namespace Modules\FrontEnd\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use App\Repositories\PageCategoriesRepository;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Modules\AdminLte\Repositories\PostsRepository;

class FrontEndServiceProvider extends ServiceProvider
{
    protected $pageCategoryRepo;
    protected $postsRepository;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot(PageCategoriesRepository $pageCategoryRepo, PostsRepository $postsRepository)
    {
        $this->pageCategoryRepo = $pageCategoryRepo;
        $this->postsRepository = $postsRepository;

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();

        $pageLocale = LaravelLocalization::getCurrentLocale();

        $menuConditions = array(
            'is_menu' => config('constants.IS_MENU')
        );

        $pageMenu = $this->pageCategoryRepo->orderBy('orderBy', 'asc')->findWhere($menuConditions,array('id','name'.(($pageLocale == 'en')?'_en':'').' as name','slug'));

        $subPosts = array();

        foreach($pageMenu as $key=>$menuItem) {
            $posts = $this->postsRepository->getPostByConditions(array("posts.page_type"=>$menuItem->id, "post_languages.lang_code"=>$pageLocale));

            $tmpAr = array();
            foreach ($posts as $post) {
                if(strpos($post->attribute, config('constants.IS_HOT')) !== false) {
                    $post['is_hot'] = true;
                }

                if(strpos($post->attribute, config('constants.IS_NEW')) !== false) {
                    $post['is_new'] = true;
                }

                if($post->type == 0) {
                    $tmpAr[$post->id] = $post;
                }
                else {
                    $tmpAr[$post->type]['hasChildren'] = true;
                    $subPosts[$post->type][] = $post;
                }
            }
            $menuItem['postSlug'] = isset($posts[0])?$posts[0]->slug:'';
            $menuItem['posts'] = $tmpAr;
            $pageMenu[$key] = $menuItem;
        }

        view()->share('pageTitle','Liệu pháp trị Chứng Vẹo cột sống & điều trị chỉnh hỉnh CLEAR Việt Nam | Bones and Beyond');
        view()->share('pageLocale',$pageLocale);
        view()->share('pageMenu',$pageMenu);
        view()->share('subPosts',$subPosts);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('frontend.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'frontend'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/frontend');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/frontend';
        }, \Config::get('view.paths')), [$sourcePath]), 'frontend');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/frontend');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'frontend');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'frontend');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
