@extends('frontend::main.index_sub')

@section('content')
    <div class='toplead'>
        @lang("frontend::main.menu_text")
    </div>
    <div class='mainimg_scoliosis'>
        <div class='inner sub-banner-{{$pageLocale}}'></div>
    </div>

    <div class='bcontents2'>
        <div class='pk'><a href='{{url('/')}}'>Top</a> > <a href='{{route('pageView', [$postDetails->category_slug, isset($navigation) ? $navigation->slug : '', $navigation->id])}}'>{{$pageLocale == 'en' ? $postDetails->category_name_en : $postDetails->category_name}}</a>  > {{$postDetails->name}} </div>

        <h1>{{$postDetails->name}}</h1>
        @if(empty($postDetails->content))
            {{trans('front.updating_content')}}
        @else
            {!! $postDetails->content !!}
        @endif
    </div>
@stop
