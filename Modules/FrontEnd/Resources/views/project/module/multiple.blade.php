<link rel='stylesheet' id='elementor-post-8-css'  href='{{asset('/wp-content/plugins/css/post-52.css')}}' type='text/css' media='all' />
<div id="content" class="content-wrapper clearfix">

    <div class="page-content clearfix">
        <div class="elementor elementor-52">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section data-id="frjuank" class="elementor-element elementor-element-frjuank elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="cifkzdu" class="elementor-element elementor-element-cifkzdu elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="ddnucwf" class="elementor-element elementor-element-ddnucwf the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">
                                                            {{isset($options['project_title_top'])? $options['project_title_top']: 'Client Album'}}
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="iklmcba" class="elementor-element elementor-element-iklmcba the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">
                                                            {{isset($options['project_title_bottom'])? $options['project_title_bottom']: 'The Memories Of Our Clients'}}
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="uijistv" class="elementor-element elementor-element-uijistv extra-padding-use mix-filter-use elementor-widget elementor-widget-nikah-portfolio-block" data-element_type="nikah-portfolio-block.default">
                                                <div class="elementor-widget-container">

                                                    <div id="some-1522970568" class="portfolio-grid-block porf-hidetitle-st clearfix">

                                                        <div class="filter-wraper">
                                                            <div id="mobile-filter-id" class="mobile-filter container clearfix">
                                                                <button id="filter-icon">
                                                                    <span class="bar bar-1"></span>
                                                                    <span class="bar bar-2"></span>
                                                                    <span class="bar bar-3"></span>
                                                                    <span class="bar bar-4"></span>
                                                                </button>
                                                            </div>
                                                            <ul id="portfolio-filter" class="filters container style-1 clearfix">
                                                                <li class="activeFilter">
                                                                    <a data-filter="*" href="#" class="filter-btn">@lang('front.all')</a>
                                                                </li>
                                                                @foreach($projects as $v)
                                                                    <li><a data-filter=".{{$v->slug}}" href="#"  class="filter-btn">{{$v->name}}</a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        <!-- #portfolio-filter end -->

                                                        <div class="portfolio-block-wrap grid-template porto-grid-wrap-loop-infinte-post-list column-4 clearfix" style="position: relative; height: 645px;">

                                                            @foreach($albums as $v)
                                                            <div class="portfo-block-item grid-item mobile-column-1 tablet-column-2 {{$v->project_slug}}" style="position: absolute; left: 0px; top: 0px;">
                                                                <div class="item-wrap">
                                                                    <a href="{{route('project', [$v->slug, $v->id])}}">
                                                                        <h3 class="portfolio-loop-title-mobile">{{$v->name}}</h3>

                                                                        <figure class="imghvr-image-rotate-left">
                                                                            <img src="{{$v->images}}" alt="Sarah &amp; Adam" width="800" height="600">
                                                                            <figcaption>
                                                                                <div class="caption-inside" style="transform: translate(-50%, -50%);">

                                                                                    <!-- if use title -->
                                                                                    <h3 class="portfolio-loop-title ih-fade-down ih-delay-sm">{{$v->name}}</h3>
                                                                                    <!-- if use title -->

                                                                                    <!-- if use category -->
                                                                                    <h5 class="portfolio-category ih-fade-up ih-delay-sm">{{$v->project_name}}</h5>
                                                                                    <!-- if use category -->

                                                                                </div>
                                                                            </figcaption>
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                        </div>


                                                        <div class="container clearfix">
                                                            {{--<div class="navigation-paging infinite-wrap style-2 clearfix">--}}
                                                                {{--<div id="load-more-loop-grid-1-loop-infinte-post-list" class="infinite-button">--}}
                                                                    {{--<a href="http://nikah1.themesawesome.com/project/page/2/"></a>			</div>--}}
                                                                {{--<button id="load-infinite-loop-grid11" class="btn">Load More</button>--}}
                                                            {{--</div>--}}
                                                        </div>

                                                    </div>
                                                    <script type="text/javascript">
                                                        (function($) {
                                                            'use strict';

                                                            $(document).ready(function(){

                                                                $('#some-1522970568 figcaption .caption-inside').css('transform', 'translate(-50%, -50%)');

                                                                var $grid = $('.grid-template').imagesLoaded( function() {
                                                                    // init Masonry after all images have loaded
                                                                    $grid.isotope({
                                                                        transitionDuration: '0.65s',
                                                                        initLayout: true,
                                                                        columnWidth: '.grid-item',
                                                                        itemSelector: '.grid-item',
                                                                        fitWidth: true,
                                                                        stagger: 30,
                                                                    });
                                                                });

                                                                var $container = $('.porto-grid-wrap-loop-infinte-post-list');

                                                                // Infinite Scroll
                                                                $container.infinitescroll({
                                                                            loading: {
                                                                                finishedMsg: 'There is no more',
                                                                                img: "http://nikah1.themesawesome.com/wp-content/themes/nikah/img/loader.gif",
                                                                                msgText: 'loading',
                                                                                speed: 'normal'
                                                                            },

                                                                            state: {
                                                                                isDone: false
                                                                            },
                                                                            navSelector  : '#load-more-loop-grid-1-loop-infinte-post-list',
                                                                            nextSelector : '#load-more-loop-grid-1-loop-infinte-post-list a',
                                                                            itemSelector : '.porto-grid-wrap-loop-infinte-post-list .grid-item',

                                                                        },
                                                                        function( newElements ) {
                                                                            var $newElems = $( newElements ).css({ opacity: 0 });
                                                                            $newElems.imagesLoaded(function(){
                                                                                $newElems.animate({ opacity: 1 });
                                                                                $container.isotope( 'appended', $newElems, true );
                                                                            });
                                                                            $('.grid-template').css('margin-bottom', 0);
                                                                        });

                                                                $container.infinitescroll('unbind');
                                                                $("#load-infinite-loop-grid11").click(function(){
                                                                    $container.infinitescroll('retrieve');
                                                                    $('.grid-template').css('margin-bottom', 120);
                                                                    return false;

                                                                });


                                                                $('#portfolio-filter a').click(function(){
                                                                    $('#portfolio-filter li').removeClass('activeFilter');
                                                                    $(this).parent('li').addClass('activeFilter');
                                                                    var selector = $(this).attr('data-filter');
                                                                    $grid.isotope({ filter: selector });
                                                                    return false;
                                                                });
                                                            });

                                                        })( jQuery );
                                                    </script><!-- Portfolio Script End -->


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section data-id="bezduef" class="elementor-element elementor-element-bezduef elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">

                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="aoxbjzr" class="elementor-element elementor-element-aoxbjzr elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="sinmjfb" class="elementor-element elementor-element-sinmjfb the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">
                                                            {{isset($options['slogan'])? $options['slogan']: 'Write your special wishes. We love to hear from all of you...	'}}
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="jjkgewx" class="elementor-element elementor-element-jjkgewx elementor-align-center animated elementor-widget elementor-widget-button fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="{{route('contact')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
                                                            <span class="elementor-button-content-wrapper"></span>
                                                            <span class="elementor-button-text">@lang('front.contact')</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div><!-- page-content -->
</div>