<section id="content" class="single-portfolio-wrap clearfix">
    <div class="single-porto-inner-wrap portfolio-chocolat portfolio-style1">

        <div class="container">

            <div class="portfolio-thumbnail standard-thumb clearfix">

                @foreach($album->sub_images as $v)
                    <div class="gallery-item item column column-{{count($album->sub_images)}}" data-src="{{$v->url}}"
                         data-sub-html="andre-hunter-264450" style="width: calc(100% / {{count($album->sub_images)}} - 20px); padding: 10px 10px">
                        <img src="{{$v->url}}" alt="">
                    </div>
                @endforeach
            </div>

            <div class="porfolio-content-wrap clearfix">
                <div class="project-details column column-3 clearfix">
                    <div class="category-portfolio">
                        <span class="category">{{$album->project->name}}</span>
                    </div>
                    <div class="page-title">
                        <h2>{{$album->name}}</h2>
                    </div>

                    <div>
                        {!! $album->description !!}
                    </div>
                </div>

                <div class="portfolio-content column column-2of3 clearfix">
                    {!! $album->content !!}

                </div>
            </div>

            {{--<div class="portfolio-pagination row">--}}

                {{--<div class="prev-portfolio column column-3">--}}
                    {{--<h3>--}}
                        {{--<a href="http://nikah1.themesawesome.com/portfolio/bella-famico/">Prev</a>--}}
                    {{--</h3>--}}
                {{--</div>--}}

                {{--<div class="all-portfolio column column-3">--}}
                    {{--<h3>--}}
                        {{--<a href="http://nikah1.themesawesome.com/project/">Back</a>--}}
                    {{--</h3>--}}
                {{--</div>--}}

                {{--<div class="next-portfolio column column-3">--}}
                    {{--<h3>--}}
                        {{--<a href="http://nikah1.themesawesome.com/portfolio/sarah-adam/">Next</a>--}}
                    {{--</h3>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>


</section>