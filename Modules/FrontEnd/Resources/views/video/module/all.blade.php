<link rel='stylesheet' id='elementor-post-8-css'  href='{{asset('/wp-content/plugins/css/post-84.css')}}' type='text/css' media='all' />

<div id="content" class="content-wrapper clearfix">

    <div class="page-content clearfix">
        <div class="elementor elementor-84">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section data-id="jxf7mr8" class="elementor-element elementor-element-jxf7mr8 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="zei11pf" class="elementor-element elementor-element-zei11pf elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="zrlezro" class="elementor-element elementor-element-zrlezro the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">@lang('front.video')</h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="legbbyi" class="elementor-element elementor-element-legbbyi the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">{{$categoryTitle}}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="de6cmny" class="elementor-element elementor-element-de6cmny extra-padding-use elementor-widget elementor-widget-nikah-post-block" data-element_type="nikah-post-block.default">
                                                <div class="elementor-widget-container">
                                                    <div class="blog-section masonry-post post-masonry-style masonry-style-1 clearfix">
                                                        <ul class="grid effect-3" id="grid" style="position: relative; height: 1691.27px;">

                                                            @foreach($videos as $v)
                                                            <li id="post-81" class="blog-item post selected-for-margin-bottom selector-padding column-3 tablet-column-2 mobile-column-1" style="position: absolute; left: 0px; top: 0px;">

                                                                <div class="loop-content">
                                                                    <div class="thumbnail">
                                                                        <a href="{{route('news', [$v->slug, $v->id])}}">
                                                                            <iframe width="100%" height="328" id="ytvideo" frameborder="0"
                                                                                    allowfullscreen src="http://www.youtube.com/embed/{{$v->ytb_url}}?autoplay=0"></iframe>

                                                                        </a>
                                                                    </div><!-- thumbnail-->

                                                                    <div class="info">


                                                                        <h4 class="title"><a href="{{route('video_detail', [$v->slug, $v->id])}}">{{$v->name}}</a></h4>
                                                                        <div class="date">{{$v->updated_at}}</div>
                                                                        <div class="post-excerpt post-text">
                                                                            {!! $v->description !!}
                                                                        </div>
                                                                        <div class="more-button clearfix">
                                                                            <a href="{{route('video_detail', [$v->slug, $v->id])}}" title="{{$v->name}}" class="more">
                                                                                @lang('front.view_detail')
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- post-content -->

                                                            </li><!-- #post-81 -->
                                                            @endforeach

                                                        </ul>

                                                    </div>

                                                    <script type="text/javascript">
                                                        (function($) {
                                                            'use strict';

                                                            $(document).ready(function(){

                                                                var $grid = jQuery('#grid').imagesLoaded( function() {
                                                                    // init Masonry after all images have loaded
                                                                    $grid.masonry({
                                                                        initLayout: true,
                                                                        columnWidth: '.blog-item',
                                                                        itemSelector: '.blog-item',
                                                                    });
                                                                });

                                                                new AnimOnScroll( document.getElementById( 'grid' ), {
                                                                    minDuration : 0.4,
                                                                    maxDuration : 0.7,
                                                                    viewportFactor : 0.2
                                                                } );



                                                            });
                                                        })( jQuery );
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>



                    <section data-id="pmzjuct" class="elementor-element elementor-element-pmzjuct elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="tnvatvr" class="elementor-element elementor-element-tnvatvr elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="havidvg" class="elementor-element elementor-element-havidvg the-title-center animated elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-center clearfix">
                                                        <h2 class="the-title">
                                                            {{isset($options['slogan'])? $options['slogan']: 'Write your special wishes. We love to hear from all of you...	'}}
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="hmhrrwa" class="elementor-element elementor-element-hmhrrwa elementor-align-center animated elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="{{route('contact')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.contact')</span>
		</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div><!-- page-content -->
</div>