<aside id="primary-sidebar" class="sidebar column column-3 clearfix has-thumb">
    <div class="blog-sidebar">

        <div id="nikah_recentpost_thumb-widget-2" class="widget widget_nikah_news"><h4 class="widget-title"><span>@lang('front.news')</span></h4>
            <ul class="nav nav-tabs clearfix" role="tablist" id="newsTabs">
                <li role="presentation" class="active"><a href="#default" aria-controls="default" role="tab" data-toggle="tab" aria-expanded="true">@lang('front.latest')</a></li>
                <li role="presentation"><a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">@lang('front.mostly')</a></li>
            </ul>

            <div class="tab-content clearfix">
                <div role="tabpanel" class="recent-news tab-pane active" id="default">
                @foreach($latest as $v)
                    <!-- widget-news -->
                        <div class="post-item clearfix">
                            <a href="{{route('news', [$v->slug, $v->id])}}">
                                <div class="post-thumb">
                                    <img src="{{$v->images}}" alt="latestwid-img">
                                    <div class="overlay"></div>
                                </div>
                            </a>
                            <div class="post-content">
                                <h5><a href="{{route('news', [$v->slug, $v->id])}}">{{$v->name}}</a></h5>
                            </div>
                        </div>
                        <!-- widget-news end -->
                    @endforeach
                </div>

                <div role="tabpanel" class="popular-news tab-pane" id="popular">
                @foreach($mostly as $v)
                    <!-- widget-news -->
                        <div class="post-item clearfix">
                            <a href="{{route('news', [$v->slug, $v->id])}}">
                                <div class="post-thumb">
                                    <img src="{{$v->images}}" alt="latestwid-img">
                                    <div class="overlay"></div>
                                </div>
                            </a>
                            <div class="post-content">
                                <h5><a href="{{route('news', [$v->slug, $v->id])}}">{{$v->name}}</a></h5>
                            </div>
                        </div>
                        <!-- widget-news end -->
                @endforeach

                </div>
            </div>

        </div>
        <div id="recent-posts-2" class="widget widget_recent_entries">
            <h4 class="widget-title"><span>@lang('front.recently')</span></h4>
            <ul>
                @foreach($recently as $v)
                <li>
                    <a href="{{route('news', [$v->slug, $v->id])}}">{{$v->name}}</a>
                </li>
                @endforeach
            </ul>
        </div>
        <div id="categories-2" class="widget widget_categories">
            <h4 class="widget-title"><span>@lang('front.category')</span></h4>
            <ul>
                @foreach($categories as $v)
                <li class="cat-item cat-item-21"><a href="{{route('news_category', [$v->slug, $v->id])}}">{{$v->name}}</a>
                </li>
                @endforeach

            </ul>

    </div>
</aside><!-- #primary-sidebar -->
