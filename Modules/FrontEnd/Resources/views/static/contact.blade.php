@extends('frontend::main.project')
@section('content')
    <div id="content" class="content-wrapper clearfix">

        <div class="page-content clearfix">
            <div class="elementor elementor-29">
                <div class="elementor-inner">
                    <div class="elementor-section-wrap">
                        <section data-id="677a1a7b" class="elementor-element elementor-element-677a1a7b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="5f0a97c7" class="elementor-element elementor-element-5f0a97c7 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="579dcb57" class="elementor-element elementor-element-579dcb57 elementor-widget elementor-widget-google_maps" data-element_type="google_maps.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-custom-embed">
                                                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=Jalan%20Pirus%2C%20Cisaranten%20Kulon%2C%20Arcamanik%2C%20Bandung%2C%20West%20Java%2C%20Indonesia&amp;t=m&amp;z=16&amp;output=embed&amp;iwloc=near"></iframe></div>		</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section data-id="344346be" class="elementor-element elementor-element-344346be elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="46d3b5d6" class="elementor-element elementor-element-46d3b5d6 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="28a3ac97" class="elementor-element elementor-element-28a3ac97 elementor-view-default elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-element_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                <span class="elementor-icon elementor-animation-">
                    <i class="fa fa-map-marker"></i>
                </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title">
                                                                    <span>Address</span>
                                                                </h3>
                                                                <p class="elementor-icon-box-description">{{isset($options['address'])? $options['address']: ''}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-id="6c34115b" class="elementor-element elementor-element-6c34115b elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="617183b" class="elementor-element elementor-element-617183b elementor-view-default elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-element_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                <span class="elementor-icon elementor-animation-">
                    <i class="fa fa-phone"></i>
                </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title">
                                                                    <span>Phone</span>
                                                                </h3>
                                                                <p class="elementor-icon-box-description">
                                                                    Hotline: {{isset($options['hotline'])? $options['hotline']: ''}}<br>
                                                                    Telephone: {{isset($options['phone'])? $options['phone']: ''}}<br>
                                                                    Fax: {{isset($options['fax'])? $options['fax']: ''}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-id="785808cf" class="elementor-element elementor-element-785808cf elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="7a794924" class="elementor-element elementor-element-7a794924 elementor-view-default elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-element_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                <span class="elementor-icon elementor-animation-">
                    <i class="fa fa-envelope"></i>
                </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title">
                                                                    <span>Email</span>
                                                                </h3>
                                                                <p class="elementor-icon-box-description">E-Mail:
                                                                    <a style="color: #F4AD20;" href="">{{isset($options['email'])? $options['email']: ''}}</a><br>
                                                                    &amp; include details about your day
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section data-id="32f87c7b" class="elementor-element elementor-element-32f87c7b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div data-id="6350b30" class="elementor-element elementor-element-6350b30 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div data-id="74852f7" class="elementor-element elementor-element-74852f7 the-title-center the-title-center animated elementor-widget elementor-widget-nikah-head-title fadeInUp" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="head-title head-title-2 text-center clearfix">
                                                            <h2 class="the-title">
                                                                Feedback	</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-id="1b5d241" class="elementor-element elementor-element-1b5d241 elementor-widget elementor-widget-nikah-text" data-element_type="nikah-text.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="nikah-text clearfix">
                                                            <p>We would love to hear from you! Contact us directly filling this form.</p></div>		</div>
                                                </div>
                                                <div data-id="6ecf3d12" class="elementor-element elementor-element-6ecf3d12 elementor-widget elementor-widget-nikah-contact-form" data-element_type="nikah-contact-form.default">
                                                    <div class="elementor-widget-container">

                                                        <div class="nikah-contact-form">
                                                            <div role="form" class="wpcf7" id="wpcf7-f6-p29-o1" lang="en-US" dir="ltr">
                                                                <div class="screen-reader-response"></div>
                                                                <form action="/contact/#wpcf7-f6-p29-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                                    <div style="display: none;">
                                                                        <input type="hidden" name="_wpcf7" value="6">
                                                                        <input type="hidden" name="_wpcf7_version" value="5.0.1">
                                                                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                                                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6-p29-o1">
                                                                        <input type="hidden" name="_wpcf7_container_post" value="29">
                                                                    </div>
                                                                    <div class="contact-form-style-1 clearfix">
                                                                        <div class="contact-bordered item-column clearfix">
                                                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name"></span> <span class="contact-ef border-form-top"><br>
</span><br>
                                                                            <span class="contact-ef border-form-left"><br>
</span><br>
                                                                            <span class="contact-ef border-form-bottom"><br>
</span><br>
                                                                            <span class="contact-ef border-form-right"><br>
</span>
                                                                        </div>
                                                                        <div class="contact-bordered item-column clearfix">
                                                                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email"></span> <span class="contact-ef border-form-top"><br>
</span><br>
                                                                            <span class="contact-ef border-form-left"><br>
</span><br>
                                                                            <span class="contact-ef border-form-bottom"><br>
</span><br>
                                                                            <span class="contact-ef border-form-right"><br>
</span>
                                                                        </div>
                                                                        <div class="contact-bordered item-column clearfix">
                                                                            <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject"></span> <span class="contact-ef border-form-top"><br>
</span><br>
                                                                            <span class="contact-ef border-form-left"><br>
</span><br>
                                                                            <span class="contact-ef border-form-bottom"><br>
</span><br>
                                                                            <span class="contact-ef border-form-right"><br>
</span>
                                                                        </div>
                                                                        <div class="contact-bordered item-column text-area clearfix">
                                                                            <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Text"></textarea></span> <span class="contact-ef border-form-top"><br>
</span><br>
                                                                            <span class="contact-ef border-form-left"><br>
</span><br>
                                                                            <span class="contact-ef border-form-bottom"><br>
</span><br>
                                                                            <span class="contact-ef border-form-right"><br>
</span>
                                                                        </div>
                                                                        <div class="contact-submit clearfix">
                                                                            <input type="submit" value="Leave Message" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span> </div>
                                                                    </div>
                                                                    <div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </div><!-- page-content -->
    </div>
@endsection