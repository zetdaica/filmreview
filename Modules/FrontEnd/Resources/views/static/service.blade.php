<section id="content" class="single-portfolio-wrap clearfix">
    <div class="single-porto-inner-wrap portfolio-chocolat portfolio-style1">

        <div class="container">

            <div class="portfolio-thumbnail standard-thumb clearfix">

                    <div class="gallery-item item column column-1" data-src="{{$service->images}}"
                         data-sub-html="andre-hunter-264450" style="width: 100%; padding: 10px 10px">
                        <img src="{{$service->images}}" alt="">
                    </div>
            </div>

            <div class="porfolio-content-wrap clearfix">
                <div class="project-details column column-3 clearfix">

                    <div class="page-title">
                        <h2>{{$service->name}}</h2>
                    </div>

                    <div>
                        {!! $service->description !!}
                    </div>
                </div>

                <div class="portfolio-content column column-2of3 clearfix">
                    {!! $service->content !!}

                </div>
            </div>

        </div>
    </div>


</section>