<div class="elementor-container elementor-column-gap-no" style="padding-top: 80px">
    <div class="elementor-row">
        <div data-id="mmdlumg"
             class="elementor-element elementor-element-mmdlumg elementor-column elementor-col-100 elementor-top-column"
             data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <section data-id="nidpjiz"
                             class="elementor-element elementor-element-nidpjiz elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                             data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="pyosbhu"
                                     class="elementor-element elementor-element-pyosbhu elementor-column elementor-col-50 elementor-inner-column"
                                     data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="f300hlz"
                                                 class="elementor-element elementor-element-f300hlz the-title-left animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                 data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                 data-element_type="nikah-head-title.default">
                                                <div class="elementor-widget-container">
                                                    <div class="head-title head-title-2 text-left clearfix">
                                                        <h2 class="the-title">
                                                            @lang('front.video') </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-id="ibmp9qn"
                                                 class="elementor-element elementor-element-ibmp9qn elementor-widget elementor-widget-nikah-text"
                                                 data-element_type="nikah-text.default">
                                                <div class="elementor-widget-container">
                                                    <div class="nikah-text clearfix">
                                                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed--}}
                                                            {{--do eiusmod tempor incididunt ut labore et dolore magna--}}
                                                            {{--aliqua.</p>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="dqnitbc"
                                     class="elementor-element elementor-element-dqnitbc elementor-column elementor-col-50 elementor-inner-column"
                                     data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="nyqfewi"
                                                 class="elementor-element elementor-element-nyqfewi elementor-align-right elementor-mobile-align-left animated fadeInUp elementor-invisible elementor-widget elementor-widget-button"
                                                 data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                 data-element_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="{{route('video_all')}}"
                                                           class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.view_all')</span>
		</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section data-id="usqvnpe"
                             class="elementor-element elementor-element-usqvnpe elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                             data-element_type="section">
                        <div class="elementor-container elementor-column-gap-no">
                            <div class="elementor-row">
                                <div data-id="qupcqji"
                                     class="elementor-element elementor-element-qupcqji elementor-column elementor-col-100 elementor-inner-column"
                                     data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div data-id="6zjo058"
                                                 class="elementor-element elementor-element-6zjo058 horizontal-item-left extra-padding-use elementor-widget elementor-widget-nikah-post-block"
                                                 data-element_type="nikah-post-block.default">
                                                <div class="elementor-widget-container">
                                                    <div class="blog clearfix">
                                                        <div class="blog-section blog-style-1-block content-section clearfix">

                                                            @foreach($videos as $v)
                                                            <article
                                                                    class="blog-item selector-padding column-4 tablet-column-2 mobile-column-1">

                                                                <div class="post-thumb">
                                                                    <iframe width="100%" height="328" id="ytvideo" frameborder="0"
                                                                            allowfullscreen src="http://www.youtube.com/embed/{{$v->ytb_url}}?autoplay=0"></iframe>
                                                                </div><!-- thumbnail-->

                                                                <div class="post-content-wrap">
                                                                    <div class="post-content">

                                                                        <!-- if use meta -->
                                                                        <div class="meta-wrapper clearfix">
                                                                                <span class="date no-author"><span>posted on</span>
                                                                                <a href="{{route('video_detail', [$v->slug, $v->id])}}">

                                                                                <span>{{$v->updated_at}}</span>
                                                                                </a>
                                                                            </span>
                                                                        </div>
                                                                        <!-- if use meta -->

                                                                        <!-- if use title -->
                                                                        <h2 class="post-title">
                                                                            <a href="{{route('video_detail', [$v->slug, $v->id])}}">{{$v->name}}</a>
                                                                        </h2>

                                                                        <div class="separator-line"><span></span></div>
                                                                        <!-- if use title -->

                                                                        <!-- if use excerpt -->
                                                                        <div class="post-text">
                                                                            {!! $v->description !!}
                                                                        </div>
                                                                        <!-- if use excerpt -->

                                                                        <!-- if use read_more -->
                                                                        <a href="{{route('video_detail', [$v->slug, $v->id])}}"
                                                                           class="read-more">@lang('front.view_detail')</a>
                                                                        <!-- if use read_more -->
                                                                    </div>
                                                                </div>
                                                            </article>

                                                            @endforeach

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
