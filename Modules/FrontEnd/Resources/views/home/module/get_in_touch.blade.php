<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="eilqgcy" class="elementor-element elementor-element-eilqgcy elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="knvgt7e" class="elementor-element elementor-element-knvgt7e the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['slogan'])? $options['slogan']: 'Write your special wishes. We love to hear from all of you...	'}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div data-id="qvdmiqd" class="elementor-element elementor-element-qvdmiqd elementor-align-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="button.default">
                        <div class="elementor-widget-container">
                            <div class="elementor-button-wrapper">
                                <a href="{{route('contact')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.contact')</span>
		</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>