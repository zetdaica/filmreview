<div class="elementor-background-overlay"></div>
<div class="elementor-shape elementor-shape-top" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"/>
        <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"/>
        <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"/>
    </svg>		</div>
<div class="elementor-shape elementor-shape-bottom" data-negative="false">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
        <path class="elementor-shape-fill" d="M0,6V0h1000v100L0,6z"/>
    </svg>		</div>
<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="hcgyn2d" class="elementor-element elementor-element-hcgyn2d elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="rkhqqtc" class="elementor-element elementor-element-rkhqqtc the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['middle_text_top'])? $options['middle_text_top']: 'We are going to...'}}
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div data-id="rr6laid" class="elementor-element elementor-element-rr6laid the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-center clearfix">
                                <h2 class="the-title">
                                    {{isset($options['middle_text_bottom'])? $options['middle_text_bottom']: 'Celebrate Our Love'}}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>