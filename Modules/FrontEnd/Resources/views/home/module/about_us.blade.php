<div class="elementor-container elementor-column-gap-no">
    <div class="elementor-row">
        <div data-id="uufxyjq" class="elementor-element elementor-element-uufxyjq elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="4vw85gf" class="elementor-element elementor-element-4vw85gf the-title-left animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-left clearfix">
                                {{--<h2 class="the-title">--}}
                                    {{--{{isset($options['about_us_menu'])? $options['about_us_menu']: @trans('front.about_us')}}--}}
                                {{--</h2>--}}
                            </div>
                        </div>
                    </div>
                    <div data-id="b0k0cm1" class="elementor-element elementor-element-b0k0cm1 the-title-left animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-head-title.default">
                        <div class="elementor-widget-container">
                            <div class="head-title head-title-2 text-left clearfix">
                                <h2 class="the-title">
                                    {{isset($options['about_us_bottom'])? $options['about_us_bottom']: ''}}
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div data-id="qfdjkcv" class="elementor-element elementor-element-qfdjkcv elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
            <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                    <div data-id="qzog4cw" class="elementor-element elementor-element-qzog4cw animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-text" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="nikah-text.default">
                        <div class="elementor-widget-container">
                            <div class="nikah-text clearfix">
                                {!! $aboutUs->top_text !!}
                            </div>
                        </div>
                    </div>
                    <div data-id="qyyqawa" class="elementor-element elementor-element-qyyqawa elementor-align-left elementor-mobile-align-left animated fadeInUp elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-element_type="button.default">
                        <div class="elementor-widget-container">
                            <div class="elementor-button-wrapper">
                                <a href="{{route('about_us')}}" class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">@lang('front.view_detail')</span>
		            </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
