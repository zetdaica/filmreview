<section id="content" class="single-post-wrap clearfix">

    <!-- BLOG START
    ============================================= -->
    <div class="blog right-sidebar clearfix">
        <div class="container clearfix">
            <div class="row clearfix">

                <!-- BLOG LOOP START
                ============================================= -->
                <div class="column column-2of3 clearfix">
                    <div class="blog-single content-section">

                        <article id="post-78" class="blog-item hentry post-78 post type-post status-publish format-standard has-post-thumbnail category-pre-wedding tag-blog tag-post tag-standard ">

                            <div class="post-content-wrap">
                                <div class="post-content">
                                    <div class="post-thumb">
                                        <img width="900" height="1342" src="{{$detail->images}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                             srcset="{{$detail->images}} 900w, {{$detail->images}} 201w, {{$detail->images}} 768w, {{$detail->images}} 687w" sizes="(max-width: 900px) 100vw, 900px">
                                    </div><!-- thumbnail-->


                                    <div class="content-inner-wrapper clearfix">
                                        <div class="meta-wrapper clearfix">

                                            <h1 class="post-title entry-title"><a href="">{{$detail->name}}</a></h1>

                                            <div class="post-meta clearfix">
                                                <div class="meta-info">
                                                    <span class="standard-post-categories">
								                    <span>in</span>
								                    <a href="{{route('news_category', [$detail->category->slug, $detail->category->id])}}" rel="category tag">{{$detail->category->name}}</a>							</span>
                                                    <span class="date">
								                    <span>Posted On</span>
								                    <a href="#"><span>November</span> <span>22</span>, <span>2017</span></a>
							                        </span>
                                                </div>
                                            </div>

                                            <div class="separator-line"><span></span></div>

                                        </div>

                                        <div class="post-text entry-content">
                                            <div>
                                                {!! $detail->content !!}
                                            </div>
                                            <div class="meta-bottom clearfix">
                                                <div class="tag-wrapper"><a href="#" rel="tag">blog</a><a href="#" rel="tag">post</a><a href="#" rel="tag">standard</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="next-prev-post standard-1 clearfix">--}}
                                {{--<div class="row">--}}


                                    {{--<div class="prev-post column column-2">--}}
                                        {{--<p><i class="icon-arrow-left"></i>Previous Post</p>--}}
                                        {{--<h4 class="title">--}}
                                            {{--<a href="http://nikah1.themesawesome.com/maecenas-ultrices-justo-metus-quis-facilisis/">Maecenas ultrices justo metus quis facilisis</a>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}

                                    {{--<div class="next-post column column-2">--}}
                                        {{--<p>Next Post<i class="icon-arrow-right"></i></p>--}}
                                        {{--<h4 class="title">--}}
                                            {{--<a href="http://nikah1.themesawesome.com/quisque-non-fermentum-proin-orci-quis-est/">Quisque non fermentum Proin orci quis est.</a>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                            {{--</div>--}}
                            <!-- pagination end -->

                        </article><!-- #post-78 -->



                    </div>
                </div>

                <!-- BLOG LOOP END -->

                <!-- SIDEBAR START
                ============================================= -->
                @include('frontend::news.module.sidebar')
                <!-- SIDEBAR END -->

            </div>
        </div>
    </div>
    <!-- BLOOG END -->

</section>