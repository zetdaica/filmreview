<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en-US" > <!--<![endif]-->

<head>
    @include('frontend::main.head')
</head>

<body id="body" class="nikah-portfolio-template-default single single-nikah-portfolio postid-114 single-format-standard header-style-1 elementor-default">

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<div id="main-wrapper" class="main-wrapper clearfix">

    @include('frontend::main.menu_fixed')

    @yield('content')

    <!-- FOOTER -->

    @include('frontend::main.footer')

    <!-- FOOTER END -->

</div>
<!-- MAIN WRAPPER END -->

@include('frontend::main.footer_script')

</body>


</html>