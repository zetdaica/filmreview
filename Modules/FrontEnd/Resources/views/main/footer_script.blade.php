<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/nikah1.themesawesome.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/css/scripts.js?ver=5.0.1'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/fitvids.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/wow.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/easing.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/smartmenus.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/owlcarousel.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/infinitescroll.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/isotope.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/headroom.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/animeonscroll.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/bootstrap.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/lightgallery.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/smoothscroll.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/stickykit.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/thumbsplugin.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/main.js'></script>
<script type='text/javascript' src='/wp-content/themes/nikah/js/header1.js'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/position.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/css/dialog.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/css/waypoints.min.js'></script>
<script type='text/javascript' src='/wp-content/plugins/css/swiper.jquery.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var elementorFrontendConfig = {"isEditMode":"","settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"is_rtl":"","urls":{"assets":"http:\/\/nikah1.themesawesome.com\/wp-content\/plugins\/elementor\/assets\/"},"post":{"id":8,"title":"Home","excerpt":""}};
    /* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/css/frontend.min.js'></script>