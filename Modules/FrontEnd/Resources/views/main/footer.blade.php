<footer id="footer" class="footer clearfix">
    <div class="footer-wrap clearfix">

        <div class="footer-bottom clearfix">
            <div class="container">
                <div class="row">
                    <div class="foot-col column column-4 item-col-3 vertical text-right clearfix" >
                        <div class="logo-footer foot-col-item" >
                            <img src="{{$logo->images}}" alt="" width="270">
                        </div>
                    </div>
                    <div class="foot-col column column-2 item-col-2 vertical text-left clearfix">
                        <div id="copyright" class="copyright-text foot-col-item">
                            <ul style="list-style-type: none">
                                <li><span class="box-name">@lang('front.address') :</span><span>{{isset($options['address'])? $options['address']: ''}}</span></li>
                                <li><span class="box-name">@lang('front.hotline') :</span><span>{{isset($options['hotline'])? $options['hotline']: ''}}</span></li>
                                <li>© Copyright 2018</li>

                            </ul>

                        </div>
                    </div>

                    <div class="foot-col column column-4 item-col-3 vertical text-right clearfix">
                        @if(isset($options['facebook']))
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fphimtruongalibaba&tabs=timeline%2C%20events%2C%20messages&width=400&height=228&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1644013652495081" width="100%" height="228" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        @endif
                    </div>


                </div>
                {{--<div class="row">--}}
                    {{--<div class="foot-col column column-3 item-col-3 vertical text-right clearfix">--}}
                        {{--<div class="social-footer foot-col-item">--}}
                            {{--<ul>--}}
                                {{--<li class="twitter soc-icon"><a target="_blank" href="{{isset($options['twitter'])? $options['twitter']: 'https://www.twitter.com/'}}" title="Twitter" class="icon icon-themify-13"></a></li>--}}
                                {{--<li class="facebook soc-icon"><a target="_blank" href="{{isset($options['facebook'])? $options['facebook']: 'https://www.facebook.com/'}}" title="Facebook" class="icon icon-themify-17"></a></li>--}}
                                {{--<li class="pinterest soc-icon"><a target="_blank" href="{{isset($options['pinterest'])? $options['pinterest']: 'https://www.pinterest.com/'}}" title="Pinterest" class="icon icon-themify-9"></a></li>--}}
                                {{--<li class="instagram soc-icon"><a target="_blank" href="{{isset($options['instagram'])? $options['instagram']: 'https://www.instagram.com/'}}" title="Instagram" class="icon icon-social-instagram-outline"></a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</footer>
<style>
    .suntory-alo-phone.suntory-alo-green .suntory-alo-ph-circle {
        border-color: #eda689;
        opacity: 1;
    }
    .suntory-alo-ph-circle {
        animation: 1.2s ease-in-out 0s normal none infinite running suntory-alo-circle-anim;
        background-color: transparent;
        border: 2px solid rgba(30,30,30,.4);
        border-radius: 100%;
        height: 70px;
        left: 13px;
        opacity: .1;
        top: 12px;
        width: 70px;
    }
    .suntory-alo-ph-circle, .suntory-alo-ph-circle-fill {
        transition: all .5s ease 0s;
        position: absolute;
        transform-origin: 50% 50% 0;
    }
    .suntory-alo-phone.suntory-alo-green .suntory-alo-ph-circle-fill {
        background-color: rgba(0,175,242,.9);
    }
    .suntory-alo-ph-img-circle {
        border: 2px solid transparent;
        border-radius: 100%;
        height: 45px;
        left: 25px;
        opacity: .7;
        position: absolute;
        top: 24px;
        transform-origin: 50% 50% 0;
        width: 45px;
        background-color: #d2581b;
    }
    .contact-popup-5chau i {
        color: #fff;
        font-size: 26px;
        /* top: 75px; */
        right: 50px;
        border: 2px solid transparent;
        border-radius: 100%;
        height: 45px;
        line-height: 45px;
        transform-origin: 50% 50% 0;
        width: 45px;
        text-align: center;
        animation: 1s ease-in-out 0s normal none infinite running suntory-alo-circle-img-anim;

    }
    @keyframes suntory-alo-circle-anim {
        0% {
            opacity: .1;
            transform: rotate(0) scale(.5) skew(1deg)
        }
        30% {
            opacity: .5;
            transform: rotate(0) scale(.7) skew(1deg)
        }
        100% {
            opacity: .6;
            transform: rotate(0) scale(1) skew(1deg)
        }
    }

    @keyframes suntory-alo-circle-img-anim {
        0%, 100%, 50% {
            transform: rotate(0) scale(1) skew(1deg)
        }
        10%, 30% {
            transform: rotate(-25deg) scale(1) skew(1deg)
        }
        20%, 40% {
            transform: rotate(25deg) scale(1) skew(1deg)
        }
    }

    @keyframes suntory-alo-circle-fill-anim {
        0%, 100% {
            opacity: .2;
            transform: rotate(0) scale(.7) skew(1deg)
        }
        50% {
            opacity: .2;
            transform: rotate(0) scale(1) skew(1deg)
        }
    }
    @keyframes suntory-alo-ring-ring {
        0%, 100%, 50% {
            transform: rotate(0) scale(1) skew(1deg)
        }
        10%, 30% {
            transform: rotate(-25deg) scale(1) skew(1deg)
        }
        20%, 40% {
            transform: rotate(25deg) scale(1) skew(1deg)
        }
    }

    /*Back to Top*/
    .cd-top {
        display: inline-block;
        height: 40px;
        width: 40px;
        position: fixed;
        right: 60px;
        bottom: 5px;
        -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);
        /* image replacement properties */
        overflow: hidden;
        text-indent: 100%;
        white-space: nowrap;
        background: rgba(232, 98, 86, 0.8) url({{asset('ui/cd-top-arrow.svg')}}) no-repeat center 50%;
        visibility: hidden;
        opacity: 0;
        -webkit-transition: opacity .3s 0s, visibility 0s .3s, background-color .3s 0s;
        transition: opacity .3s 0s, visibility 0s .3s, background-color .3s 0s;
    }

    .cd-top.cd-top--show,
    .cd-top.cd-top--fade-out,
    .cd-top:hover {
        -webkit-transition: opacity .3s 0s, visibility 0s 0s, background-color .3s 0s;
        transition: opacity .3s 0s, visibility 0s 0s, background-color .3s 0s;
    }

    .cd-top.cd-top--show {
        /* the button becomes visible */
        visibility: visible;
        opacity: 1;
    }

    .cd-top.cd-top--fade-out {
        /* if the user keeps scrolling down, the button is out of focus and becomes less visible */
        opacity: .5;
    }

    .cd-top:hover {
        background-color: #e86256;
        opacity: 1;
    }

    @media only screen and (min-width: 768px) {
        .cd-top {
            right: 60px;
            bottom: 5px;
        }
    }

    @media only screen and (min-width: 1024px) {
        .cd-top {
            height: 45px;
            width: 45px;
            right: 60px;
            bottom: 5px;
        }
    }

</style>
<div class="contact-popup-5chau">
    <a href="tel:{{isset($options['hotline'])? $options['hotline']: '0909587557'}}" class="suntory-alo-phone suntory-alo-green" title="Liên hệ với chúng tôi." style="right: 130px; top: 60px; position: fixed;"><p></p>
        <div class="suntory-alo-ph-circle"></div>
        <div class="suntory-alo-ph-circle-fill"></div>
        <div class="suntory-alo-ph-img-circle"><i class="fa fa-phone"></i>
        </div>
        <p></p>
    </a>
</div>

<a href="#0" class="cd-top js-cd-top">Top</a>
<script>
    (function(){
        var backTop = document.getElementsByClassName('js-cd-top')[0],
                // browser window scroll (in pixels) after which the "back to top" link is shown
                offset = 300,
                //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
                offsetOpacity = 1200,
                scrollDuration = 700
        scrolling = false;
        if( backTop ) {
            //update back to top visibility on scrolling
            window.addEventListener("scroll", function(event) {
                if( !scrolling ) {
                    scrolling = true;
                    (!window.requestAnimationFrame) ? setTimeout(checkBackToTop, 250) : window.requestAnimationFrame(checkBackToTop);
                }
            });
            //smooth scroll to top
            backTop.addEventListener('click', function(event) {
                event.preventDefault();
                (!window.requestAnimationFrame) ? window.scrollTo(0, 0) : scrollTop(scrollDuration);
            });
        }

        function checkBackToTop() {
            var windowTop = window.scrollY || document.documentElement.scrollTop;
            ( windowTop > offset ) ? addClass(backTop, 'cd-top--show') : removeClass(backTop, 'cd-top--show', 'cd-top--fade-out');
            ( windowTop > offsetOpacity ) && addClass(backTop, 'cd-top--fade-out');
            scrolling = false;
        }

        function scrollTop(duration) {
            var start = window.scrollY || document.documentElement.scrollTop,
                    currentTime = null;

            var animateScroll = function(timestamp){
                if (!currentTime) currentTime = timestamp;
                var progress = timestamp - currentTime;
                var val = Math.max(Math.easeInOutQuad(progress, start, -start, duration), 0);
                window.scrollTo(0, val);
                if(progress < duration) {
                    window.requestAnimationFrame(animateScroll);
                }
            };

            window.requestAnimationFrame(animateScroll);
        }

        Math.easeInOutQuad = function (t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t + b;
            t--;
            return -c/2 * (t*(t-2) - 1) + b;
        };

        //class manipulations - needed if classList is not supported
        function hasClass(el, className) {
            if (el.classList) return el.classList.contains(className);
            else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
        }
        function addClass(el, className) {
            var classList = className.split(' ');
            if (el.classList) el.classList.add(classList[0]);
            else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
            if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
        }
        function removeClass(el, className) {
            var classList = className.split(' ');
            if (el.classList) el.classList.remove(classList[0]);
            else if(hasClass(el, classList[0])) {
                var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                el.className=el.className.replace(reg, ' ');
            }
            if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
        }
    })();
</script>