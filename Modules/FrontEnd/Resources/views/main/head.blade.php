<title>{{$pageTitle}}</title>
<meta charset="UTF-8">
<!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
@if(!empty($icon))
    <link rel="icon" href="{{$icon->images}}" type="image/x-icon">
@endif
<script type="text/javascript">
    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/nikah1.themesawesome.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
    !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel='stylesheet' id='contact-form-7-css'  href='{{asset('wp-content/plugins/css/styles.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='{{asset('wp-content/plugins/css/settings.css')}}' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
    #rs-demo-id {}
</style>
<link rel='stylesheet' id='nikah-plugin-css-css'  href='{{asset('wp-content/themes/nikah/css/plugin.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='nikah-style-css'  href='{{asset('wp-content/themes/nikah/style.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='nikah-font-css'  href='{{asset('wp-content/themes/nikah/css/font.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='nikah-responsive-css-css'  href='{{asset('wp-content/themes/nikah/css/responsive.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='nikah-custom-style-css'  href='{{asset('wp-content/themes/nikah/css/custom-style.css')}}' type='text/css' media='all' />
<style id='nikah-custom-style-inline-css' type='text/css'>

</style>

<link rel='stylesheet' id='elementor-icons-css'  href='{{asset('wp-content/plugins/css/elementor-icons.min.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='{{asset('wp-content/plugins/css/font-awesome.min.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'  href='{{asset('wp-content/plugins/css/animations.min.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='{{asset('wp-content/plugins/css/frontend.min.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='site-categories-styles-css'  href='{{asset('wp-content/plugins/css/site-categories-styles.css')}}' type='text/css' media='all' />
<script type='text/javascript' src='{{asset('wp-content/themes/nikah/js/jquery.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-includes/js/jquery/jquery-migrate.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-includes/js/imagesloaded.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-includes/js/masonry.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/plugins/css/nikah-instafeed.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/plugins/css/jquery.themepunch.tools.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/plugins/css/jquery.themepunch.revolution.min.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/themes/nikah/js/modernizr.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/themes/nikah/js/respond.js')}}'></script>
<script type='text/javascript' src='{{asset('wp-content/themes/nikah/js/classie.js')}}'></script>
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">

<style type="text/css" id="wp-custom-css">
    .meta.meta-comments a.comments span, .icon-simple-line-icons-124:before {
        color: #ffffff;
    }
    .contact-map iframe {
        height: 520px;
    }		</style>
<style type="text/css" title="dynamic-css" class="options-output">
    body .alt-head .main-menu ul.sm-clean>li>a, body .alt-head .search-wrap #btn-search i, body .alt-head .main-menu ul.sm-clean>li.current-menu-item>a, .alt-head .site-title a{color:#ffffff;}
    #header #primary-menu li a, #header #secondary-menu li a{font-family:Poppins,Georgia, serif;text-align:inherit;line-height:25.6px;font-weight:500;font-style:normal;font-size:16px;}
    .blog-content-wrap .blog{padding-top:60px;padding-bottom:60px;}.archive #content{padding-top:60px;padding-bottom:60px;}
    .single-post-wrap .blog{padding-top:0;padding-bottom:150px;}.single-portfolio-wrap{padding-top:20px;padding-bottom:60px;}
    #footer .footer-widget-wrapper{padding-top:0;padding-bottom:0;}#footer .footer-bottom{padding-top:40px;padding-bottom:10px;}
    .footer-widget-wrapper{border-top:0px solid #efefef;}.footer-bottom{border-top:0px solid #efefef;}
</style>
@if(isset($options['facebook']))
    <style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 50px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{display: none; width: 120px; left: -140px; top: 5px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/phimtruongalibaba" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="http://phimtruongalibaba.vn/" target="_blank">Powered by Phim Trường Alibaba</a> </div><div id="fb-root"></div></div><a href="https://m.me/phimtruongalibaba" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Bạn cần hỗ trợ?</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script>$(document).ready(function(){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>
@endif
<style>
    .ctrlq.fb-button{
        width: 50px;
        height: 50px;
        right: 60px;
        bottom: 65px;
    }
</style>

<link rel='stylesheet'  href='{{asset('css/frontend.css')}}' type='text/css' media='all' />
