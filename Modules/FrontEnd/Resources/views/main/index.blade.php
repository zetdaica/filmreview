<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en-US" > <!--<![endif]-->

<head>
    @include('frontend::main.head')
</head>

<body id="body" class="home page-template page-template-template page-template-page-builder-template page-template-templatepage-builder-template-php page page-id-8 header-style-1 elementor-default elementor-page elementor-page-8">

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<div id="main-wrapper" class="main-wrapper clearfix">

@include('frontend::main.menu')

<!-- HEADER END -->
    <!-- CONTENT WRAPPER
    ============================================= -->
    <div id="content" class="content-wrapper clearfix">

        <div class="page-content clearfix">
          @yield('content')
        </div><!-- page-content -->
    </div>
    <!-- #content-wrapper end -->

    <!-- FOOTER -->

@include('frontend::main.footer')

<!-- FOOTER END -->

</div>
<!-- MAIN WRAPPER END -->

{{--<link rel='stylesheet' property='stylesheet' id='rs-icon-set-fa-icon-css'  href='/wp-content/plugins/revslider/public/assets/fonts/font-awesome/css/font-awesome.css' type='text/css' media='all' />--}}

@include('frontend::main.footer_script')

</body>
</html>