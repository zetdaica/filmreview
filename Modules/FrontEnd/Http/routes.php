<?php
Route::group(['prefix' => '','middleware' => [ 'web'] ,'namespace' => 'Modules\FrontEnd\Http\Controllers'], function()
{
    Barryvdh\Debugbar\Facade::disable();

    Route::get('/', 'FrontEndController@index')->name('home');
    Route::get('/home', 'FrontEndController@index');
    Route::get('/about_us', 'FrontEndController@aboutUs')->name('about_us');
    Route::get('/contact', 'FrontEndController@contact')->name('contact');
    Route::get('/search', 'FrontEndController@search')->name('search');
    Route::get('/service/{slug}/{id}', 'FrontEndController@service')->name('service');

    Route::group(['prefix'=>'project'],function(){
        Route::get('/', 'FrontEndController@projectAll')->name('project_all');
        Route::get('/{slug}/{id}', 'FrontEndController@project')->name('project');
    });

    Route::group(['prefix'=>'news'],function(){
        Route::get('/', 'FrontEndController@newsAll')->name('news_all');
        Route::get('/{slug}/{id}', 'FrontEndController@news')->name('news');
        Route::get('/category/{slug}/{id}', 'FrontEndController@newsCategory')->name('news_category');

    });
    Route::group(['prefix'=>'video'],function(){
        Route::get('/', 'FrontEndController@videoAll')->name('video_all');
        Route::get('/{slug}/{id}', 'FrontEndController@videoDetail')->name('video_detail');
    });
});
