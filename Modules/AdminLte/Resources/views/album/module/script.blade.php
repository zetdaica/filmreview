<style>
    .sub-image {
        width: 150px;
    }
</style>
<script>

    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            editRow: {},
            editId: '',
            selected: [],
            title: '',
            btnSave : '',
            slug : 'abc',
            status: '{{!empty($detail)? $detail->status : 1}}',
            pageType: null,
            postType : '',
            imgCount: '{{!empty($subs)? count($subs) + 1 : 1}}',

        },
        computed:{

        },
        created:function () {
            this.init();
        },

        methods:{
            init: function () {


            },
            saveForm: function () {
                var self = this;

                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        location.href = '{{route('backend.album.index', $projectId)}}';
                    }else{
                        self.notify(data.message,'danger');

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },

            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success
                });
            },

            pageTypeChange: function (pageType) {
                var self = this;

                this.ajaxRequest("GET", '{{route('backend.post.get_post_by_page_type')}}', {page_type: pageType}, function (data){
                    if(data.success){
                        self.pageType = data.result;
                        $('select[name="type"]').val(null).change();
                    }
                });
            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, catId: this.catId}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Xóa thành công '+self.selected.length+' item !', 'success');

                        }else{
                            for(var o in data.message) {
                                self.notify(data.message[o][0],'danger');
                            }
                        }
                    });
                }else{

                }

            },
            numberToString: function (number) {
                return (number + "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            },
            addLibraryImg: function () {
                var div = this.getImageHtml(this.imgCount);
                $("#library-frame").append(div);
                $('.lfm').filemanager('image');
                this.imgCount ++;

            },
            getImageHtml : function (number) {
                var img = 'img_' + number;
                var div = '<div class="form-group lib-group">';
                div +='<label for="' + img + '" class="col-sm-2 control-label"><button type="button" onclick="deleteSub(this)" class="btn btn-danger"><i class="fa fa-remove"></i></button></label>';
                div +='<div class="col-sm-7">';
                div +='<div class="input-group">';
                div +='<span class="input-group-btn">';
                div +='<a data-input="'+img+'" data-preview="'+img+'_holder" class="btn btn-primary lfm">';
                div +='<i class="fa fa-picture-o"></i> Choose';
                div +='</a>';
                div +='</span>';
                div +='<input style="width: 150px;" id="'+img+'" class="form-control sub-image" type="text" name="sub[]">';
                div +='</div>';
                div +='</div>';
                div +='<div class="col-sm-7">';
                div +='<img id="'+img+'_holder" v-bind:src="'+img+'" width="100%">';
                div +='</div>';
                div +='</div>';
                return div;
            },
        }

    });
    function pageTypeChange(e) {
        tableContent.pageTypeChange($(e).val());
    }
    function deleteSub(e) {
        $(e).parents('.lib-group').remove();
    }

    $('.lfm').filemanager('image');

</script>