<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
{{--{{dd($cat->FKCategory)}}--}}
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" href="{{$routeAdd}}">
                            <i class="fa fa-plus-square-o"></i> Thêm
                        </a>

                        <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i> Xóa
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Tên</th>
                                <th>Hình ảnh</th>
                                <th>Trạng thái</th>
                                <th>Thứ tự</th>
                                <th>Ngày tạo</th>
                                <th>Ngày cập nhật</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($rows as $v)
                                <tr>
                                    <td><input type="checkbox" class="checklist" v-model="selected" value="{{$v->id}}" data-item="{{$v->id}}"></td>
                                    <td>
                                        <a href="{{route('backend.banner.edit', $v->id)}}" class="btn btn-info btn-sm" ><i class="fa fa-edit"></i></a>
                                        <a href="{{route('backend.banner.delete', $v->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></a>
                                    </td>
                                    <td><a href="{{route('backend.banner.edit', $v->id)}}">{{$v->name}}</a></td>
                                    <td><img height="80" src="{{$v->images}}"></td>

                                    <td>
                                        <div class="form-group">
                                            @if($v->status == 1)
                                                <button type="button" class="btn btn-success btn-sm disabled" >Active</button>
                                            @else
                                                <button type="button" class="btn btn-default btn-sm disabled" >Inactive</button>
                                            @endif
                                        </div>
                                    </td>
                                    <td> {{$v->order}}</td>
                                    <td> {{$v->created_at}}</td>
                                    <td> {{$v->updated_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</section>




