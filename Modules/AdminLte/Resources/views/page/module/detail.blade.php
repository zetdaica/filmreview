<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="saveForm">
                            <i class="fa fa-edit"></i> Lưu
                        </a>
                        {{--<a class="btn btn-app" @click="deleteSelected">--}}
                            {{--<i class="fa fa-remove"></i> Xóa--}}
                        {{--</a>--}}
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                        <i class="fa fa-undo"></i> Hủy
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <form v-bind:detail="detail = true" id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                                    {{csrf_field()}}
                                    <div class="box-header with-border">
                                        <h3 class="box-title">@{{ title }} </h3>
                                        <button type="button" class="btn btn-default pull-right btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                                        <button type="button" class="btn btn-success pull-right btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-sm-12">
                                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}
                                            {!! FormHelpers::inputHidden("active", 'editRow.active') !!}
                                            {!! FormHelpers::inputHidden("FKCategory", 'editRow.FKCategory') !!}

                                            {!! FormHelpers::input("Tên", "name", 'editRow.name') !!}
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Trạng thái</label>
                                                <div class="col-sm-10">
                                                    <button  v-if="editRow.active == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.active = 0">Active</button>
                                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.active = 1">Inactive</button>
                                                </div>
                                            </div>
                                            {!! FormHelpers::input("Thứ tự", "orderBy", 'editRow.orderBy') !!}
                                            {!! FormHelpers::image("Hình ảnh", "images", 'editRow.images') !!}
                                            {!! FormHelpers::textArea("Mô tả", "description", 'editRow.description') !!}
                                            {!! FormHelpers::textArea("Nội dung", "content", 'editRow.content') !!}

                                        </div>

                                    </div>
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>




