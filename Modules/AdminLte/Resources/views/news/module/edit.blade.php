<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="saveForm">
                        <i class="fa fa-edit"></i> Lưu
                        </a>
                        {{--<a class="btn btn-app" @click="deleteSelected">--}}
                        {{--<i class="fa fa-remove"></i> Xóa--}}
                        {{--</a>--}}
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                        <i class="fa fa-undo"></i> Hủy
                        </a>
                        <a class="btn btn-app btn-danger" href="{{route('backend.news.index' , $newsId)}}">
                            <i class="fa fa-undo"></i> Quay lại
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                                    {{csrf_field()}}

                                    <div class="box-body">
                                        <div class="col-sm-12">
                                            {!! FormHelpers::inputHiddenDefault("id", $detail->id) !!}
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.name')}}</label>
                                                        <div class="col-sm-6">
                                                            <input type="text" name="name" class="form-control" value="{{$detail->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.status')}}</label>
                                                        <div class="col-sm-10">
                                                            <input type="hidden" name="status" value="" v-model="status">
                                                            <button  v-if="status == 1"  type="button" class="btn btn-success btn-sm" @click="status = 0">Active</button>
                                                            <button v-else type="button" class="btn btn-default btn-sm" @click="status = 1">Inactive</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.is_home')}}</label>
                                                        <div class="col-sm-10">
                                                            <input type="hidden" name="is_home" value="" v-model="is_home">
                                                            <button  v-if="is_home == 1"  type="button" class="btn btn-success btn-sm" @click="is_home = 0">On</button>
                                                            <button v-else type="button" class="btn btn-default btn-sm" @click="is_home = 1">Off</button>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.project')}}</label>
                                                        <div class="col-sm-3">
                                                            <select name="category_id" class="form-control select2 select2-hidden-accessible" style="width: 100%">
                                                                @foreach($categories as $v)

                                                                    <option value="{{$v->id}}" @if($v->id == $newsId) selected @endif>{{$v->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.order')}}</label>
                                                        <div class="col-sm-2">
                                                            <input type="number" name="order" class="form-control" value="{{$detail->order}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">{{trans('backend.attribute')}}</label>
                                                        <div class="col-sm-4">
                                                            @foreach($attributeType as $v)
                                                                @if(empty($detail->attribute))
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input name="attribute[]" value="{{$v->paramID}}" type="checkbox">
                                                                            {{$v->value}}
                                                                        </label>
                                                                    </div>
                                                                @else
                                                                    <?php
                                                                    $attribute = explode(',', $detail->attribute);

                                                                    ?>

                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input name="attribute[]" value="{{$v->paramID}}" type="checkbox" @if(in_array($v->paramID, $attribute)) checked @endif>
                                                                            {{$v->value}}
                                                                        </label>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    {!! FormHelpers::textAreaDefault("Mô tả", "description", $detail->description) !!}
                                                    {!! FormHelpers::textAreaDefault("Nội dung", "content", $detail->content) !!}

                                                </div>

                                                <div class="col-sm-4">
                                                    {!! FormHelpers::imageDefault(trans('backend.images'), "images", $detail->images) !!}

                                                </div>
                                            </div>



                                        </div>

                                    </div>
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>




