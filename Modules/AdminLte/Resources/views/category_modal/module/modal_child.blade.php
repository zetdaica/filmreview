<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">@{{ title }}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form id="formSubmit" action="{{route('backend.category.save')}}" method="post"
                                      class="form-horizontal">
                                    {{csrf_field()}}

                                    <div class="box-body">
                                        <input name="id" type="hidden" v-model="editRow.id">
                                        <input name="parent" type="hidden" value="{{$cat->id}}">

                                        <div class="form-group">
                                            <label for="input1" class="col-sm-2 control-label">Danh mục cấp 1 :</label>
                                            <div class="col-sm-10">
                                                <label class="control-label">{{$cat->name}}</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input1" class="col-sm-2 control-label">Tên danh mục</label>
                                            <div class="col-sm-10">
                                                <input name="name" type="text" class="form-control" id="input1"
                                                       placeholder="" v-model="editRow.name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="input2" class="col-sm-2 control-label">EN</label>
                                            <div class="col-sm-10">
                                                <input name="name_en" type="text" class="form-control" id="input2"
                                                       placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Thứ tự</label>
                                            <div class="col-sm-2">
                                                <input name="orderBy" type="number" class="form-control" id="input3"
                                                       placeholder="" value="1">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Trạng thái</label>
                                            <div class="col-sm-2">
                                                <input v-if="editRow.active == '1'" name="active"
                                                       class="toggle-one toggle-check" checked type="checkbox">
                                                <input v-else name="active" class="toggle-one toggle-check"
                                                       type="checkbox">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="input3" class="col-sm-2 control-label">Hình ảnh</label>
                                            <div class="col-sm-7">
                                                <div class="input-group">
                                                          <span class="input-group-btn">
                                                            <a id="lfm" data-input="thumbnail" data-preview="holder"
                                                               class="btn btn-primary">
                                                              <i class="fa fa-picture-o"></i> Choose
                                                            </a>
                                                          </span>
                                                    <input id="thumbnail" class="form-control" type="text" name="images"
                                                           v-model="editRow.images">

                                                </div>


                                            </div>
                                            <div class="col-sm-4 col-md-offset-2">
                                                <img id="holder" width="100%" src="" v-model="editRow.images">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="textarea" class="col-sm-2 control-label">Nội dung</label>
                                            <div class="col-sm-10 ">
                                                <textarea class="description" name="description"
                                                          v-model="editRow.description"></textarea>

                                            </div>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-primary" @click="saveForm"><i class="fa fa-save"></i></button>
            </div>
        </div>
    </div>
</div>
