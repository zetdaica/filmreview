<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="saveForm">
                        <i class="fa fa-edit"></i> Lưu
                        </a>
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                        <i class="fa fa-remove"></i> Hủy
                        </a>


                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                                    {{csrf_field()}}

                                    <div class="box-body">
                                        <div class="col-sm-12">
                                            {!! FormHelpers::inputHiddenDefault("id", $detail->id) !!}

                                            <div class="col-sm-6">

                                                {!! FormHelpers::imageOther(trans('backend.top_image'), "top_image", $detail->top_image) !!}
                                                {!! FormHelpers::textAreaDefault(trans('backend.top_text'), "top_text", $detail->top_text) !!}
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">{{trans('backend.name')}}</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="name" class="form-control" value="{{$detail->name}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                {!! FormHelpers::imageOther(trans('backend.middle_image'), "middle_image", $detail->middle_image) !!}
                                                {!! FormHelpers::textAreaDefault(trans('backend.middle_text'), "middle_text", $detail->middle_text) !!}
                                            </div>

                                        </div>

                                    </div>
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>




