
<!-- Bootstrap 3.3.5 -->
<script src="{{asset('backend/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('backend/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend/plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/app.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('backend/dist/js/demo.js')}}"></script>

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


<script>
    function initResources() {
        $('textarea.ckeditor-main').ckeditor({
            filebrowserImageBrowseUrl: '{{url('/')}}/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '{{url('/')}}/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: '{{url('/')}}/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '{{url('/')}}/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',
            format_tags : 'p;h1;h2;h3;pre;div',
            extraPlugins : 'justify'

        });
        $('.image1').filemanager('image');
        $('.toggle-one').bootstrapToggle();
        $(".select2").select2();

    }
    $(document).ready(function(){
        initResources();
    });
</script>
