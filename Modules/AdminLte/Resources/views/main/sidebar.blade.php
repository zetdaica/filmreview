<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @if(Auth::check())
                <p>{{Auth::user()->name}}</p>
                @endif
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li>
            </li>
            <li class="header">MAIN NAVIGATION</li>

            <li class="treeview @if($current == "option") active @endif">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>{{trans('backend.config')}}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->role == 0)
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.option.index')}}"><i class="fa fa-calendar"></i> <span>All</span></a>
                    </li>
                    @endif
                    @foreach($configType as $c)
                        <li>
                            <a href="#"><i class="fa fa-calendar"></i> <span>{{$c->value}}</span></a>
                            <ul class="treeview-menu">
                                @foreach($options as $v)
                                    @if($v->type == $c->paramID)
                                    <li @if(url()->current() == route('backend.option.detail', [$v->id])) class="active" @endif>
                                        <a href="{{route('backend.option.detail', $v->id)}}"><i class="fa fa-circle-o"></i> {{$v->name}}</a>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @endforeach


                </ul>
            </li>
            @if(Auth::user()->role == 0)
            <li @if(url()->current() == route('backend.parameter.index')) class="active" @endif>
                <a href="{{route('backend.parameter.index')}}"><i class="fa fa-calendar"></i> <span>Parameter</span></a>
            </li>
            @endif


            {{--<li @if(url()->current() == route('backend.page.category.index')) class="active" @endif>--}}
                {{--<a href="{{route('backend.page.category.index')}}"><i class="fa fa-user"></i> <span>{{@trans('backend.post_category')}}</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            @if(Auth::user()->role == 0)
            <li @if(url()->current() == route('backend.post.index')) class="active" @endif>
                <a href="{{route('backend.post.index')}}"><i class="fa fa-user"></i> <span>{{@trans('backend.posts')}}</span></a>
            </li>
            @endif

            {{--<li @if(url()->current() == route('backend.post.add', 0)) class="active" @endif>--}}
                {{--<a href="{{route('backend.post.add', 0)}}"><i class="fa fa-plus-square-o"></i> <span>{{@trans('backend.add_post_category')}}</span></a>--}}
            {{--</li>--}}
            <li class="treeview @if($current == "page") active @endif">
                <a href="#">
                <i class="fa fa-folder"></i> <span>{{trans('pages')}}</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li @if(url()->current() == route('backend.about_us.edit')) class="active" @endif>
                        <a href="{{route('backend.about_us.edit')}}"><i class="fa fa-plus-square-o"></i> <span>{{@trans('backend.about_us')}}</span></a>
                    </li>
                </ul>
            </li>
            <li @if(url()->current() == route('backend.banner.index')) class="active" @endif>
                <a href="{{route('backend.banner.index')}}"><i class="fa fa-film"></i> <span>{{@trans('backend.banner')}}</span></a>
            </li>
            <li @if(url()->current() == route('backend.project.index')) class="active" @endif>
                <a href="{{route('backend.project.index')}}"><i class="fa fa-paint-brush"></i> <span>{{@trans('backend.project')}}</span></a>
            </li>
            <li @if(url()->current() == route('backend.news_category.index')) class="active" @endif>
                <a href="{{route('backend.news_category.index')}}"><i class="fa fa-database"></i> <span>{{@trans('backend.news_category')}}</span></a>
            </li>
            <li @if(url()->current() == route('backend.member.index')) class="active" @endif>
                <a href="{{route('backend.member.index')}}"><i class="fa fa-cubes"></i>
                    <span>{{@trans('backend.member')}}</span></a>
            </li>
            <li @if(url()->current() == route('backend.video.index')) class="active" @endif>
                <a href="{{route('backend.video.index')}}"><i class="fa fa-cubes"></i>
                    <span>{{@trans('backend.video')}}</span></a>
            </li>
            <li @if(url()->current() == route('backend.system_feature.index')) class="active" @endif>
                <a href="#"><i class="fa fa-cubes"></i>
                    <span>{{@trans('backend.system_feature')}}</span></a>
                <ul class="treeview-menu">
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'LOGO')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.logo')</span></a>
                    </li>
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'ICON')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.icon')</span></a>
                    </li>
                </ul>
            </li>
            <li @if(url()->current() == route('backend.system_feature.index')) class="active" @endif>
                <a href="#"><i class="fa fa-cubes"></i>
                    <span>{{@trans('backend.services')}}</span></a>
                <ul class="treeview-menu">
                    @if(Auth::user()->role == 0)
                    <li @if(url()->current() == route('backend.system_feature.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.index')}}"><i class="fa fa-calendar"></i> <span>All</span></a>
                    </li>
                    @endif
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'SERVICE1')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.service1')</span></a>
                    </li>
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'SERVICE2')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.service2')</span></a>
                    </li>
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'SERVICE3')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.service3')</span></a>
                    </li>
                    <li @if(url()->current() == route('backend.option.index')) class="active" @endif>
                        <a href="{{route('backend.system_feature.oneFeature', 'SERVICE4')}}"><i class="fa fa-calendar"></i> <span>@lang('backend.service4')</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
</aside>