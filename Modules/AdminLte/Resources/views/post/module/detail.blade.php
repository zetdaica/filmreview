<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="saveForm">
                            <i class="fa fa-edit"></i>
                            <span>{{trans('backend.save')}}</span>
                        </a>
                        <a class="btn btn-app btn-danger" href="{{route('backend.post.deleteOne', $detail->id)}}">
                            <i class="fa fa-remove"></i>
                            <span>{{trans('backend.delete')}}</span>
                        </a>
                        <a class="btn btn-app btn-danger" href="{{$routeIndex}}">
                        <i class="fa fa-undo"></i>
                            <span>{{trans('backend.cancel')}}</span>
                        </a>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                                    {{csrf_field()}}

                                    <div class="box-body">
                                        {!! FormHelpers::inputHiddenDefault("id", $detail->id) !!}

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.status')}}</label>
                                                <div class="col-sm-10">
                                                    <input type="hidden" name="status" value="{{$detail->status}}" v-model="status">
                                                    <button  v-if="status == 1"  type="button" class="btn btn-success btn-sm" @click="status = 0">Active</button>
                                                    <button v-else type="button" class="btn btn-default btn-sm" @click="status = 1">Inactive</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.is_home')}}</label>
                                                <div class="col-sm-10">
                                                    <input type="hidden" name="is_home" value="{{$detail->is_home}}" v-model="is_home">
                                                    <button  v-if="is_home == 1"  type="button" class="btn btn-success btn-sm" @click="is_home = 0">On</button>
                                                    <button v-else type="button" class="btn btn-default btn-sm" @click="is_home = 1">Off</button>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.order')}}</label>
                                                <div class="col-sm-2">
                                                    <input type="number" name="order" class="form-control" value="{{$detail->order}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.page_type')}}</label>
                                                <div class="col-sm-8">
                                                    <select onchange="pageTypeChange(this)" name="page_type" class="form-control select2 select2-hidden-accessible" style="width: 100%">
                                                        <option value="" selected disabled>-- Chose --</option>
                                                        @if(!empty($catId))
                                                            @foreach($pageType as $v)
                                                                @if($v->id == $catId)
                                                                    <option value="{{$v->id}}" selected>{{$v->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @foreach($pageType as $v)
                                                                <option value="{{$v->id}}" @if($detail->page_type == $v->id) selected @endif>{{$v->name}}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.type')}}</label>
                                                <div class="col-sm-8">
                                                    <select v-if="pageType == null"  name="type" class="form-control select2 select2-hidden-accessible" style="width: 100%">
                                                        <option selected disabled>-- Chose --</option>
                                                        <option value="0" @if($detail->type == 0) selected @endif> {{trans('backend.category')}}</option>
                                                        @foreach($postType as $v)
                                                            <option value="{{$v->id}}" @if($detail->type == $v->id) selected @endif>{{$v->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <select v-model="postType" v-else name="type" class="form-control select2 select2-hidden-accessible" style="width: 100%">
                                                        <option selected disabled>-- Chose --</option>
                                                        <option v-for="v in pageType" v-bind:value="v.id">@{{ v.name }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">{{trans('backend.attribute')}}</label>
                                                <div class="col-sm-4">
                                                    @foreach($attributeType as $v)
                                                        @if(empty($detail->attribute))
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="attribute[]" value="{{$v->paramID}}" type="checkbox">
                                                                    {{$v->value}}
                                                                </label>
                                                            </div>
                                                        @else
                                                            <?php
                                                                $attribute = explode(',', $detail->attribute);

                                                            ?>

                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="attribute[]" value="{{$v->paramID}}" type="checkbox" @if(in_array($v->paramID, $attribute)) checked @endif>
                                                                    {{$v->value}}
                                                                </label>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! FormHelpers::imageDefault(trans('backend.images'), "images", $detail->images) !!}
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="nav-tabs-custom">
                                                <ul class="nav nav-tabs pull-right">
                                                    @foreach(config('constants.languages') as $k => $v)
                                                        <li @if($k == config('constants.lang_default'))class="active" @endif><a href="#tab_{{$k}}" data-toggle="tab" aria-expanded="true">{{$v}}</a></li>
                                                    @endforeach

                                                    <li class="pull-left header"><i class="fa fa-th"></i> Language Tabs</li>
                                                </ul>
                                                <div class="tab-content">
                                                    @foreach(config('constants.languages') as $k => $v)
                                                        <div class="tab-pane @if($k == config('constants.lang_default')) active @endif" id="tab_{{$k}}">
                                                            @if($create)
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">{{trans('backend.name')}}</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" name="name[{{$k}}]" class="form-control" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">{{trans('backend.sub_title')}}</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" name="sub_title[{{$k}}]" class="form-control" value="">
                                                                    </div>
                                                                </div>
                                                                {!! FormHelpers::textAreaDefault(trans('backend.description'), "description[$k]", '') !!}
                                                                {!! FormHelpers::textAreaDefault(trans('backend.content'), "content[$k]", '') !!}
                                                            @else
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">{{trans('backend.name')}}</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" name="name[{{$k}}]" class="form-control" value="{{$postLangs[$k]['name']}}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">{{trans('backend.sub_title')}}</label>
                                                                    <div class="col-sm-5">
                                                                        <input type="text" name="sub_title[{{$k}}]" class="form-control" value="{{$postLangs[$k]['sub_title']}}">
                                                                    </div>
                                                                </div>
                                                                {!! FormHelpers::textAreaDefault(trans('backend.description'), "description[$k]", $postLangs[$k]['description']) !!}
                                                                {!! FormHelpers::textAreaDefault(trans('backend.content'), "content[$k]", $postLangs[$k]['content']) !!}
                                                            @endif

                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="box-footer">
                                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i>
                                            <span>{{trans('backend.save')}}</span>
                                        </button>

                                        <button type="reset" class="btn btn-default pull-left btn-sm"><i class="fa fa-fw fa-remove"></i>
                                            <span>{{trans('backend.reset')}}</span>
                                        </button>

                                        <a href="{{route('backend.post.deleteOne', $detail->id)}}" class="btn btn-danger pull-left btn-sm" ><i class="fa fa-fw fa-save"></i>
                                            <span>{{trans('backend.delete')}}</span>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>

@section('footer_script')
    @include('adminlte::post.module.script_detail')
@endsection


