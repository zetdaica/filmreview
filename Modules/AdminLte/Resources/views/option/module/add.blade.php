<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}

                            {!! FormHelpers::input("Option ID", "optionID", 'editRow.optionID', 3) !!}

                            {!! FormHelpers::input(trans('backend.value'), "value", 'editRow.value', 3) !!}

                            {!! FormHelpers::input(trans('backend.name'), "name", 'editRow.name', 3) !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{trans('backend.type')}}</label>
                                <div class="col-sm-6">
                                    <select name="type" class="form-control select2 select2-hidden-accessible" style="width: 100%" v-model="editRow.type">
                                        @foreach($configType as $v)
                                            <option value="{{$v->paramID}}">{{$v->value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> {{trans('backend.cancel')}}</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>

                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
