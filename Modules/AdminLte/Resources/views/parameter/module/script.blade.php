<script>
    var tableContent = new Vue({
        el: "#tableContent",
        data:{
            dataJson: {},
            editRow: {},
            editId: '',
            showModal: false,
            selected: [],
            title: '',
            btnSave : '',
            action: false

        },
        computed:{
          rows: function () {
              return this.dataJson.rows;
          }
        },
        created:function () {
            this.init();
            setTimeout(function(){
                $('#dataTable').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "lengthMenu": [[5,10, 25, 50, -1],[5,10, 25, 50, "All"]]

                });

            }, 0);

        },
        methods:{
            init: function () {
                var self = this;
                this.ajaxRequest("GET", '{{$routeInit}}', {}, function (data){
                    self.dataJson =  JSON.parse(data);
                });
            },
            addNew : function () {
                this.actionForm(true);
                this.title = "{{trans('backend.add')}}";
                this.btnSave = "{{trans('backend.save')}}";
            },
            saveForm: function () {
                var self = this;
                this.ajaxRequest("POST", '{{$routeSave}}', $("#formSubmit").serialize(), function (data){
                    if(data.success){
                        self.parseData(data.result);
                        self.actionForm(false);
                        self.notify('Saving success !', 'success');

                    }else{
                        for(var o in data.message) {
                            self.notify(data.message[o][0],'danger');
                        }

                    }
                });
            },
            notify: function (mess, type) {
                $.notify({
                    // options
                    message: mess
                },{
                    // settings
                    type: type,
                    z_index: 99999
                });
            },
            deleteItem: function (id) {
                var self = this;
                this.ajaxRequest("POST", '{{$routeDelete}}', {id: id}, function (data){
                    if(data.success){
                        self.parseData(data.result);
                    }else{
                        self.notify(data.message[o][0],'danger');
                    }
                });
            },
            getUpdateItem: function (id, index, e) {
                $("#dataTable tr").removeClass('selected');
                $(e).parents('tr').addClass('selected');
                this.actionForm(true);
                this.editId = index;
                this.title = "{{trans('backend.edit')}}";
                this.btnSave = "{{trans('backend.save')}}";
                this.editRow = this.rows[index];

            },
            parseData: function (data) {

                this.dataJson =  JSON.parse(data);

            },
            ajaxRequest: function (method, url, dataSend, success) {
                var self = this;
                dataSend._token = '{{csrf_token()}}';
                $.ajax({
                    type: method,
                    url: url,
                    data: dataSend,
                    async: false,
                    success: success

                });
            },
            editSelected: function () {

                if(this.selected.length > 1){
                    this.notify('Pls select only 1 item to edit', 'warning');

                }else if(this.selected.length == 1){
                    this.getUpdateItem(this.selected[0], $(".checklist[value='"+this.selected[0]+"']").attr('data-item'));
                }else{
                    this.notify('Pls select 1 item to edit', 'warning');
                }

            },
            deleteSelected: function () {

                if(this.selected.length > 0){
                    if(!confirm("Delete :" )){
                        return;
                    }
                    var self = this;
                    this.ajaxRequest("POST", '{{$routeDelete}}', {listId: this.selected, parent: this.parent}, function (data){
                        if(data.success){
                            self.parseData(data.result);
                            self.notify('Deleting success !', 'success');

                        }else{
                            self.notify(data.message,'danger');

                        }
                    });
                }else{

                }

            },
            resetForm: function () {
                this.initRowEdit();
                if($("#formSubmit").length > 0){
                    $("#formSubmit")[0].reset();
                }
            },
            actionForm: function (type) {
                this.resetForm();
                this.action = type;
                if(type){
                    $("#blockForm").show(200);

                }else{
                    $("#blockForm").hide(200);
                    $("#dataTable tr").removeClass('selected');

                }
            },
            initRowEdit: function () {
                this.editRow = {
                    id: 0,
                    paramID: '',
                    paramCode: '',
                    value: ''
                };
            }


        }

    });
</script>