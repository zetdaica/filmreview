<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>

<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-8' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="addNew">
                            <i class="fa fa-plus-square-o"></i>
                            <span>{{trans('backend.add')}}</span>
                        </a>
                        <a class="btn btn-app" @click="editSelected">
                            <i class="fa fa-edit"></i>
                            <span>{{trans('backend.edit')}}</span>
                        </a>
                        <a class="btn btn-app" @click="deleteSelected">
                            <i class="fa fa-remove"></i>
                            <span>{{trans('backend.delete')}}</span>
                        </a>
                        <a class="btn btn-app btn-danger" @click="actionForm(false)">
                            <i class="fa fa-undo"></i>
                            <span>{{trans('backend.cancel')}}</span>
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('backend.action')}}</th>
                                <th>ID</th>
                                <th>ParamID</th>
                                <th>{{trans('backend.value')}}</th>
                                <th>Param Code</th>
                                <th>{{trans('backend.created_at')}}</th>
                                <th>{{trans('backend.updated_at')}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="(v,index) in rows">
                                <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm" @click="getUpdateItem(v.id, index, $event.target)"><i class="fa fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                                </td>
                                <td>@{{ v.id}}</td>
                                <td>@{{v.paramID}}</td>
                                <td>@{{v.value}}</td>
                                <td>@{{v.paramCode}}</td>
                                <td> @{{v.created_at}}</td>
                                <td> @{{v.updated_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            @include('adminlte::parameter.module.add')
        </div>
    </div>

</section>




