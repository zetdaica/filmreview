@extends('adminlte::main.index')

@section('content')
    @include($viewContent)
@endsection

@section('footer_script')

    @include('adminlte::project.module.script')

@endsection
