<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>

<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-8' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <form method="POST" action="{{route('backend.user.front.update')}}">
                            <div class="form-group">
                                <label class="col-sm-2">Trạng thái đơn hàng </label>
                                <div class="col-sm-2">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{$detail->id}}">
                                    <select name="active" class="form-control select2 " style="width: 100%;">
                                        <option value="">- Chose -</option>
                                        @foreach($status as $v)
                                            @if($detail->active == $v->paramID)
                                                <option value="{{$v->paramID}}" selected>{{$v->value}}</option>
                                            @else
                                                <option value="{{$v->paramID}}">{{$v->value}}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </div>
                                <label class="col-sm-2">Loại khách hàng </label>

                                <div class="col-sm-2">
                                    <select name="member" class="form-control select2 " style="width: 100%;">
                                        <option value="0">- Chose -</option>
                                            @if($detail->member == 1)
                                                <option value="1" selected>Thành viên</option>
                                                <option value="0">Thường</option>
                                            @else
                                                <option value="1">Thành viên</option>
                                                <option value="0" selected>Thường</option>
                                            @endif
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success btn-lg">
                                    <i class="fa fa-fw fa-save"></i> Lưu
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-header">

                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Mã hàng</th>
                                    <th>Tên khách hàng</th>
                                    <th>Email</th>
                                    <th>Điện thoại</th>
                                    <th style="width: 20%">Địa chỉ</th>
                                    <th>Tông tiền</th>
                                    <th>Lời nhắn KH</th>
                                    <th>Ngày đặt hàng</th>

                                </tr>
                            </thead>

                            <tbody style="font-size: 1.5em; color: green">
                                <tr>
                                    <td>{{$detail->name}}</td>
                                    <td>{{$detail->username}}</td>
                                    <td>{{$detail->email}}</td>
                                    <td>{{$detail->phone}}</td>
                                    <td>{{$detail->address}}</td>
                                    <td>{{number_format($detail->total,0,',','.')}}</td>
                                    <td>{{$detail->note}}</td>
                                    <td>{{$detail->created_at}}</td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Tên sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Giá</th>
                                <th>Hình ảnh</th>
                                <th>Tổng tiền</th>

                            </tr>
                        </thead>

                        <tbody>
                            @foreach(json_decode($detail->content, true)['content']['content'] as $v)
                            <tr>
                                <td>{{$v['name']}}</td>
                                <td>{{$v['qty']}}</td>
                                <td>{{number_format($v['price'],0,',','.') }}</td>
                                <td><img src="{{$v['options']['image']}}" height="80"></td>
                                <td>{{number_format($v['subtotal'],0,',','.')}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                <td><b>{{number_format($detail->total,0,',','.')}}</b></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            @include('adminlte::parameter.module.add')
        </div>
    </div>

</section>




