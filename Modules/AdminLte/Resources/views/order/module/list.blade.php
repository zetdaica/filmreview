<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        <small>{{$title}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#">{{$title}}</a></li>
    </ol>
</section>
<style>
.status{
    color: #ad0d0d;
}
.status-submit{
    background-color: #af9870;
}
.status-confirm{
    background-color: yellow;
}
.status-notyet{
    background-color: red;
    color: #fff;
}
.status-finish{
    background-color: #008000;
    color: #fff;
}
</style>
<!-- Main content -->
<section id="tableContent" class="content">
    <div class="row">
        <div v-bind:class="{'col-sm-8' :action , 'col-sm-12' : !action}">
            <div class="box">
                <div class="box-header">
                    <div class="box-body">
                        <a class="btn btn-app" @click="deleteSelected">
                        <i class="fa fa-remove"></i> Xóa
                        </a>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Mã hàng</th>
                            <th>loại KH</th>
                            <th>Tên khách hàng</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th style="width: 20%">Địa chỉ</th>
                            <th>Tông tiền</th>
                            <th>Trạng thái</th>
                            <th>Ngày đặt hàng</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="(v,index) in rows">
                            <td><input type="checkbox" class="checklist" v-model="selected" v-bind:value="v.id" v-bind:data-item="index"></td>
                            <td>
                                <a v-bind:href="'/admin/order/detail/'+v.id" class="btn btn-info btn-sm"><i class="fa fa-info"> Chi tiết</i></a>
                                <button type="button" class="btn btn-danger btn-sm" @click="deleteItem(v.id, v.parent)"><i class="fa fa-remove"></i></button>
                            </td>
                            <td>@{{v.name}}</td>
                            <td><span v-if="v.member != 0">@{{ v.type }}</span><span v-else>Chưa đăng ký</span></td>
                            <td>@{{v.username}}</td>
                            <td>@{{v.email}}</td>
                            <td>@{{v.phone}}</td>
                            <td>@{{v.address}}</td>

                            <td><b style="color: red">@{{numberToString(v.total)}}</b></td>
                            <td class="status"><button v-bind:class="classObj(v.active)" type="button" >@{{v.status? v.status.value : 'None_'+v.active}}</button></td>
                            <td> @{{v.created_at}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            @include('adminlte::order.module.add')
        </div>
    </div>

</section>




