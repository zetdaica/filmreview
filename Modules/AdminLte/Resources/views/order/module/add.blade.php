<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}

                            {!! FormHelpers::input("ParamID", "paramID", 'editRow.paramID') !!}

                            {!! FormHelpers::input("Giá trị", "value", 'editRow.value') !!}

                            {!! FormHelpers::input("Param Coce", "paramCode", 'editRow.paramCode') !!}

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>

                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
