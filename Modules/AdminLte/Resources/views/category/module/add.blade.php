<div class="row" id="blockForm" style="display: none;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <form id="formSubmit" action="{{$routeSave}}" method="post" class="form-horizontal">
                    {{csrf_field()}}
                    <div class="box-header with-border">
                        <h3 class="box-title">@{{ title }}</h3>
                        <button type="button" class="btn btn-default pull-right btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-right btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                    <div class="box-body">
                        <div class="col-sm-12">
                            {!! FormHelpers::inputHidden("id", 'editRow.id') !!}
                            {!! FormHelpers::inputHidden("parent", 'editRow.parent') !!}
                            {!! FormHelpers::inputHidden("active", 'editRow.active') !!}

                            {!! FormHelpers::input("Tên", "name", 'editRow.name') !!}
                            {!! FormHelpers::input("Tên (EN)", "name_en", 'editRow.name_en') !!}
                            @if(!empty($level3))
                                <input type="hidden" name="cate_3" value="1">
                            @endif
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Trạng thái</label>
                                <div class="col-sm-10">
                                    <button  v-if="editRow.active == 1"  type="button" class="btn btn-success btn-sm" @click="editRow.active = 0">Active</button>
                                    <button v-else type="button" class="btn btn-default btn-sm" @click="editRow.active = 1">Inactive</button>
                                </div>
                            </div>
                            @if(!empty($listCategories) && empty($level1))
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Danh mục</label>
                                <div class="col-sm-10">
                                    <select name="parent" class="form-control select2" style="width: 100%;">
                                        @foreach($listCategories as $v)
                                            <option data-parent="{{$v->parent}}" @if(isset($cat) && $cat->id == $v->id) selected @endif value="{{$v->id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            @endif
                            @if(!empty($level1))
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Banner</label>
                                <div class="col-sm-10">
                                    <select name="banner[]" class="form-control select2 type-display" style="width: 100%;" tabindex="-1" aria-hidden="true" multiple>
                                        <option value="" selected>-- Choose one --</option>
                                        @foreach($banners as $v)
                                            <option value="{{$v->id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {!! FormHelpers::imageOther("Icon", "thumbnail", 'editRow.thumbnail') !!}

                            @endif
                            {!! FormHelpers::input("Thứ tự", "orderBy", 'editRow.orderBy') !!}
                            {!! FormHelpers::image("Hình ảnh", "images", 'editRow.images') !!}



                            {!! FormHelpers::textArea("Mô tả", "description", 'editRow.description') !!}

                        </div>

                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" @click="actionForm(false)"><i class="fa fa-fw fa-remove"></i> Cancel</button>
                        <button type="button" class="btn btn-success pull-left btn-sm" @click="saveForm"><i class="fa fa-fw fa-save"></i> @{{ btnSave }}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
<script>
    $(document).ready(function () {
        $('.lfm').filemanager('image');
    })
</script>