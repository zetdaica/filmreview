<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class SystemFeatures extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'system_features';

    protected $fillable = [
        'id', 'slug', 'config_key',  'name', 'images','description', 'content', 'order', 'status'
    ];

}
