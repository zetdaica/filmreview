<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Orders extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id', 'optionID','value', 'name', 'active', 'created_at', 'updated_at', 'member',
    ];
    protected $table = "orders";
    public function status(){
        return $this->hasOne(Parameters::class,  'paramID', 'active');
    }
    public function user_front(){
        return $this->hasOne(UsersFront::class, 'id', 'member');
    }

}
