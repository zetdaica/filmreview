<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PageCategories extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'page_categories';

    protected $fillable = [
        'id', 'name','name_en', 'slug', 'images','description','parent', 'orderBy', 'active', 'created_at', 'updated_at',
    ];

    public function getCatLevel2(){
        return $this->hasMany(PageCategories::class, "parent", "id");
    }
    public function pages(){
        return $this->hasMany(Pages::class, 'FKCategory', 'id');
    }
}
