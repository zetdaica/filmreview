<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AboutUs extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'about_us';

    protected $fillable = [
        'id', 'name', 'top_text','top_image', 'middle_text', 'middle_image', 'members', 'clients'
    ];

}
