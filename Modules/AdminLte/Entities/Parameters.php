<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Parameters extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id', 'paramID','value', 'paramCode', 'active', 'created_at', 'updated_at',

    ];
    protected $table = "parameters";

    public static function getConfigType(){
        return self::where('paramCode', 'configType')->get();
    }

}
