<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Videos extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'videos';

    protected $fillable = [
        'id', 'ytb_url','slug', 'name', 'images','description', 'content', 'order', 'status'
    ];


}
