<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Projects extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'projects';

    protected $fillable = [
        'id', 'slug', 'name', 'images','description', 'content', 'order', 'status'
    ];

    public function albums (){
        return $this->hasMany(Albums::class, 'project_id', 'id');
    }

}
