<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductCategories extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'product_categories';

    protected $fillable = [
        'id', 'name','name_en', 'slug', 'images','description','parent',  'orderBy', 'active', 'banner', 'created_at', 'updated_at', 'thumbnail', 'level'
    ];

    public function getCatLevel2(){
        return $this->hasMany(ProductCategories::class, "parent", "id");
    }
    public function _parent(){
        return $this->hasOne(ProductCategories::class, "id", "parent");
    }

}
