<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Albums extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'albums';

    protected $fillable = [
        'id', 'project_id','slug', 'name', 'images','description', 'content', 'order', 'status'
    ];

    public function sub_images(){
        return $this->hasMany(SubImages::class, 'album_id', 'id');
    }

    public function project(){
        return $this->hasOne(Projects::class, 'id', 'project_id');
    }

}
