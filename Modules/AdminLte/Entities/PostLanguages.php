<?php

namespace Modules\AdminLte\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PostLanguages extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'post_languages';

    protected $fillable = [
        'id',
        'post_id',
        'lang_code',
        'name',
        'slug',
        'images',
        'description',
        'content',
        'sub_title',
    ];


}
