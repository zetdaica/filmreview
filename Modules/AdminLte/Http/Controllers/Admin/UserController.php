<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
Use App\Models\Users;
use Auth,Input,Validator,Redirect,Session,Response;

class UserController extends Controller
{
     public function postSignUp(Request $request) {
        $post = $request->all();
       $rules = array(
            //'email' => 'required|email',
            'password' => 'required|min:6|max:32',
        );
        $messages = array(
            'password' => array(
                'required' => 'Password bắt buộc *',
                'min' => 'Ít nhất 6 ký tự',
                'max' => 'Nhiều nhất là 32 ký tự'
            ),
        );
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return Response::json(['status' => 'error', 'message' => $validator->errors()->all()]);
        }
        $check = Users::where('username', $post['username'])->count();

        if ($check > 0) {
            return Response::json(array('status' => false, 'message' => "This usename has been !"));
            exit;
        }
        $data = array(
            'username' => strtolower($post['username']),
            'name' => $post['name'],
            'password' => $post['password'],
        );
        $rs = Users::CreateUser($data);
        return Response::json($rs);
    }

    public function postLogIn(Request $request) {
        $post = $request->all();
        dd($post);
        if (Auth::attempt(['username' => $post['username'], 'password' => $post['password']], true)) {
        dump(Auth::user());
            
            return Response::json(array('success' => true));
        } else {
            Session::flash('error', 'Email or password not correct');
            return Response::json(array('success' => false, 'message' => "Not correct"));
        }
    }

    public function getLogOut(Request $request) {
        session()->forget('user');
        return json_encode(array('success' => true));
    }

    public function validatior(array $formData) {
        $rs = new BaseForm();
        $validation = Validator::make($formData, [
                    'email' => 'required|email|max:255',
                    'password' => 'required|min:6',
        ]);

        if ($rs->validation = $validation->fails()) {
            $rs->error = $validation->errors();
        }
        return $rs;
    }
}
