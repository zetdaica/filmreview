<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\BannersRepository;
use Validator,Response;

class BannerController extends BaseController
{
    protected $bannersRepository;

    function __construct(BannersRepository $bannersRepository){
        parent::lteInit();
        $this->bannersRepository = $bannersRepository;
        $this->data['routeSave'] = route('backend.banner.save');
        $this->data['routeIndex'] = route('backend.banner.index');
        $this->data['routeAdd'] = route('backend.banner.add');
        $this->data["title"] = "Banners";
    }

    public function index(){

        $this->data["rows"] =$this->bannersRepository->all();
        $this->data['viewContent'] = 'adminlte::banner.module.list';
        return view('adminlte::banner.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::banner.module.add';

        return view('adminlte::banner.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->bannersRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::banner.module.edit';
        return view('adminlte::banner.index', $this->data);
    }
    public function delete($id){
        $this->bannersRepository->delete($id);
        return redirect()->route('backend.banner.index');
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->bannersRepository->rules(), $this->bannersRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->bannersRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $this->bannersRepository->create($data);
            } else {
                $this->bannersRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
