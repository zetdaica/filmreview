<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\AdminLte\Entities\Parameters;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Response;
use Modules\AdminLte\Repositories\OrdersRepository;
use Modules\AdminLte\Entities\Orders;

class OrderController extends BaseController
{
    protected $ordersRepository;
    const ORDER_SUBMIT = 1;
    const ORDER_CONFIRM = 2;
    function __construct(OrdersRepository $ordersRepository){
        parent::lteInit();

        $this->ordersRepository = $ordersRepository;
        $this->data['routeSave'] = route('backend.order.save');
        $this->data['routeDelete'] = route('backend.order.delete');
        $this->data['routeInit'] = route('backend.order.init');

        $this->data['current'] = '';
    }
    public function index(){
        $this->data['title'] = 'Thông tin đơn hàng';
        $this->data['dataJson'] = $this->getNewUpdate([]);
        $this->data['viewContent'] = 'adminlte::order.module.list';
        return view('adminlte::order.index', $this->data);
    }
    public function detail($id){
        $this->data['status'] = Parameters::where('paramCode','OrderStatus')->get();

        $this->data['title'] = 'Chi tiết đơn hàng';

        $this->data['detail'] = $this->ordersRepository->find($id);
//        dd($this->data['detail']);
        $this->data['viewContent'] = 'adminlte::order.module.detail';

        return view('adminlte::order.index', $this->data);

    }
    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->ordersRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Orders::whereIn('id', $req['listId'])->delete();

        }else{
            $this->ordersRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }
    public function update(Request $request){
        $param = $request->all();
        $this->ordersRepository->update(array("active" => $param['active'], 'member' => $param['member']), $param['id']);
        return redirect()->back();
    }
    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->ordersRepository->rules(), $this->ordersRepository->messages());

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->ordersRepository->find($param['id']): null;

        try {

            $data = array(
                'paramID' => $param['paramID'],
                'value' => $param['value'],
                'paramCode' => $param['paramCode'],
                'active' => 1,
            );
            if (empty($exits)) {
                $this->ordersRepository->create($data);
            } else {
                $this->ordersRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(){
        return $this->getNewUpdate([['orders.active', '<>', $this::ORDER_SUBMIT]]);
    }
    protected function getNewUpdate($cond){

        return  json_encode(array(
                'rows' => $this->ordersRepository
                    ->with(['status'])
                    ->scopeQuery(function ($q){
                        return $q->leftJoin('users_front', 'orders.member', '=', 'users_front.id')
                            ->leftJoin('parameters', 'users_front.type', '=', 'parameters.ParamID')
                            ->select('orders.*','parameters.value as type');
                    })
                    ->orderBy('orders.created_at','desc')
                    ->findWhere($cond)
                    ->toArray(),
                'rules' => $this->ordersRepository->rules()
            )
        );
    }
}
