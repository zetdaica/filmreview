<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Redirect ,Response;
use App\Repositories\PageCategoriesRepository;
use Modules\AdminLte\Entities\PageCategories;

class PageCategoryController extends BaseController
{
    public $pageCategoryRepo;
    function __construct(PageCategoriesRepository $pageCategoriesRepo){
    	parent::lteInit();
        $this->pageCategoryRepo = $pageCategoriesRepo;
        $this->data['routeSave'] = route('backend.page.category.save');
        $this->data['routeDelete'] = route('backend.page.category.delete');
        $this->data['routeInit'] = route('backend.page.category.init');
        $this->data['current'] = 'page_category';

    }
    public function index(){
        $this->data['title'] = trans('backend.post_category');

        $this->data['viewContent'] = 'adminlte::pagecategory.module.list';
        return view('adminlte::pagecategory.index', $this->data);
    }

    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->pageCategoryRepo->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            PageCategories::whereIn('id', $req['listId'])->delete();

        }else{
            $this->pageCategoryRepo->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->pageCategoryRepo->rules(), $this->pageCategoryRepo->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->pageCategoryRepo->find($param['id']): null;

        try {
            $data = array(
                'parent' => $param['parent'],
                'name' => $param['name'],
                'is_menu' => $param['is_menu'],
                'name_en' => $param['name_en'],
                'slug' => str_slug($param['name_en']),
                'images' => "",
                'description' => "",
                'active' => $param['active'],
                'orderBy' => $param['orderBy'],
            );
            if (empty($exits)) {
                $this->pageCategoryRepo->create($data);
            } else {
                $this->pageCategoryRepo->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(){
        return $this->getNewUpdate([]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
                'rows' => $this->pageCategoryRepo
                    ->orderBy('id','desc')
                    ->findWhere($cond),
                'rules' => $this->pageCategoryRepo->rules()
            )
        );
    }
}
