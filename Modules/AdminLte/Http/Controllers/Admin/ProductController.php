<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Response;
use Modules\AdminLte\Entities\Products;
use Modules\AdminLte\Repositories\ProductsRepository;
use Modules\AdminLte\Repositories\ProductCategoriesRepository;
use Modules\AdminLte\Repositories\ParametersRepository;

class ProductController extends BaseController  {

    protected $productRepository;
    protected $categoryRepository;
    protected $parametersRepository;
    function __construct(ProductsRepository $productRepository ,ProductCategoriesRepository $categoriesRepository, ParametersRepository $parametersRepository){
        parent::lteInit();

        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoriesRepository;
        $this->parametersRepository = $parametersRepository;
        $this->data['routeSave'] = route('backend.product.save');
        $this->data['routeDelete'] = route('backend.product.delete');
        $this->data['routeInit'] = route('backend.product.init');

        $this->data['current'] = 'product';
    }
    public function index(){

        $this->data['cat'] = null;
        $this->data['title'] = 'Sản phẩm';
        $this->data['listProductType'] = $this->parametersRepository->findWhere(["paramCode" => "ProductType"]);
        $this->data['categories'] = $this->categoryRepository
            ->with("getCatLevel2")
            ->findWhere(["parent" => 0], ['id','name']);
        $this->data['viewContent'] = 'adminlte::product.module.list';
        return view('adminlte::product.index', $this->data);
    }
    public function productsOfChild($catId){
        $this->data['cat'] = $this->categoryRepository->find($catId);
        $this->data['title'] = 'Sản phẩm : '.$this->data['cat']->name;
        $this->data['listProductType'] = $this->parametersRepository->findWhere(["paramCode" => "ProductType"]);
        $this->data['categories'] = $this->categoryRepository
            ->with("getCatLevel2")
            ->findWhere(["parent" => 0], ['id','name']);

        $this->data['viewContent'] = 'adminlte::product.module.list';

        return view('adminlte::product.index', $this->data);

    }
    public function productsOfChildLevel3($catId){
        $this->data['level3'] = true;

        $this->data['cat'] = $this->categoryRepository->with('_parent')->find($catId);
        $this->data['title'] = 'Sản phẩm thuộc : '.$this->data['cat']->_parent->name.' - '.$this->data['cat']->name;
        $this->data['listProductType'] = $this->parametersRepository->findWhere(["paramCode" => "ProductType"]);
        $this->data['categories'] = $this->categoryRepository
            ->findWhere(["parent" => $this->data['cat']->parent], ['id','name']);

        $this->data['viewContent'] = 'adminlte::product.module.list';

        return view('adminlte::product.index', $this->data);

    }
    public function productsDeleted(){

        $this->data['title'] = 'Sản phẩm da xoa ';
        $this->data['deleted'] = true;

        $this->data['listProductType'] = $this->parametersRepository->findWhere(["paramCode" => "ProductType"]);
        $this->data['categories'] = $this->categoryRepository
            ->with("getCatLevel2")
            ->findWhere(["parent" => 0], ['id','name']);

        $this->data['viewContent'] = 'adminlte::product.module.list';

        return view('adminlte::product.index', $this->data);

    }
    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->productRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Products::whereIn('id', $req['listId'])->delete();

        }else{
            $this->productRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate(['FKCategory' => $req['parent']])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->productRepository->rules(), $this->productRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->productRepository->find($param['id']): null;
        $thumbnails = '';
        if(!empty($param['galleries'])){
            $thumbnails = array();
            foreach ($param['galleries'] as $v){
                if($v != ''){
                    $thumbnails[] = $v;
                }
            }
        }

        try {
            $type = !empty($param['type'])? implode(',',$param['type']) : "";
            $cat = $this->categoryRepository->find($param['FKCategory']);
            $data = array(
                'FKCategory' => $param['FKCategory'],
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'thumbnail' => json_encode($thumbnails),
                'description' => $param['description'],
                'content' => $param['content'],
                'price' => stringToInterger($param['price']),
                'active' => $param['active'],
                'orderBy' => $param['orderBy'],
                'type' => $type,
                'parentID' => $cat->parent,
                'tags' => $param['tags'],
                'sales' => !empty($param['sales'])? $param['sales'] : 0,
                'others' => $this->convertArrayToString($request->get('others'))
            );
            if (empty($exits)) {
                $this->productRepository->create($data);
            } else {
                $this->productRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate(['FKCategory' => $param['FKCategory']])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(Request $request){
        return $this->getNewUpdate(['FKCategory' => $request->get('id')]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
                'rows' => $this->productRepository
                    ->orderBy('id','desc')
                    ->findWhere($cond),
                'rules' => $this->productRepository->rules()
            )
        );
    }
    protected function convertArrayToString($array){
        $str = '';
        if(empty($array)){
            return $str;
        }
        foreach ($array as $v){
            $str .= '-'.$v.'-,';
        }
        return rtrim($str,",");
    }

}
