<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\NewsCategoriesRepository;
use Modules\AdminLte\Repositories\NewsRepository;
use Validator,Response;

class NewsCategoryController extends BaseController
{
    protected $newsCategoriesRepository;
    protected $newsRepository;

    function __construct(NewsCategoriesRepository $newsCategoriesRepository, NewsRepository $newsRepository){
        parent::lteInit();
        $this->newsCategoriesRepository = $newsCategoriesRepository;
        $this->newsRepository = $newsRepository;

        $this->data['routeSave'] = route('backend.news_category.save');
        $this->data['routeIndex'] = route('backend.news_category.index');
        $this->data['routeAdd'] = route('backend.news_category.add');
        $this->data["title"] = "project";
    }

    public function index(){
        $this->data["rows"] =$this->newsCategoriesRepository->all();
        $this->data['viewContent'] = 'adminlte::news_category.module.list';
        return view('adminlte::news_category.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::news_category.module.add';
        return view('adminlte::news_category.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->newsCategoriesRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::news_category.module.edit';
        return view('adminlte::news_category.index', $this->data);
    }
    public function delete($id){
        $this->newsCategoriesRepository->delete($id);
        $this->newsRepository->deleteWhere(array(
           'category_id' => $id
        ));
        return redirect()->route('backend.news_category.index');
    }
    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->newsCategoriesRepository->rules(), $this->newsCategoriesRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->newsCategoriesRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $this->newsCategoriesRepository->create($data);
            } else {
                $this->newsCategoriesRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
