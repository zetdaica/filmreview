<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;
use App\Models\Product_categories;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Response;
use Modules\AdminLte\Entities\Pages;
use Modules\AdminLte\Repositories\ProductCategoriesRepository;
use Modules\AdminLte\Entities\PageCategories;

class CategoryController extends BaseController
{
    protected $productCategoriesRepository;
    protected $pageRepo;
    protected $productsRepository;
    const LEVEL_1 = 0;

    function __construct(ProductCategoriesRepository $productCategoriesRepository, PageCategories $pageCategories, ProductsRepository $productsRepository){
        parent::lteInit();
        $this->productCategoriesRepository = $productCategoriesRepository;
        $this->productsRepository = $productsRepository;
        $this->data['routeSave'] = route('backend.category.save');
        $this->data['routeDelete'] = route('backend.category.delete');
        $this->data['routeInit'] = route('backend.category.init');
        $this->data['current'] = '';
    }
    public function index(){
        $this->data['level1'] = true;
        $this->data['ID'] = 0;

        $this->data['title'] = "Danh mục cấp 1";
        $this->data['banners'] = Pages::with('getPageCategory')->whereHas('getPageCategory', function ($query){
            return $query->where("slug","danh-sach-banner");
        })->get();

        $this->data['viewContent'] = 'adminlte::category.module.list';
    	return view('adminlte::category.index', $this->data);
    }
    public function child($id){
        $this->data['level2'] = true;
        $this->data['ID'] = $id;

        $this->data['current'] = 'category';
        $this->data['listCategories'] = $this->productCategoriesRepository
            ->with("getCatLevel2")
            ->findWhere(["parent" => $this::LEVEL_1], ['id','name']);
        $this->data['cat'] = $this->productCategoriesRepository->findByField("id",$id)->first();
        $this->data['title'] = "Danh mục cấp 2 : ".$this->data['cat']->name;

        $this->data['viewContent'] = 'adminlte::category.module.list';
        return view('adminlte::category.index', $this->data);
    }
    public function level3($id){
        $this->data['level3'] = true;
        $this->data['ID'] = $id;

        $this->data['current'] = 'category-3';

        $this->data['cat'] = $this->productCategoriesRepository->findByField("id",$id)->first();
        $this->data['listCategories'] = $this->productCategoriesRepository
            ->findWhere(["parent" => $this->data['cat']->parent], ['id','name']);
        $this->data['title'] = "Danh mục cấp 3 : ".$this->data['cat']->name;

        $this->data['viewContent'] = 'adminlte::category.module.list';
        return view('adminlte::category.index', $this->data);
    }
    public function edit(Request $request){

        return Response::json(array('success' => true, 'result' => $this->productCategoriesRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Product_categories::whereIn('id', $req['listId'])->delete();
            foreach ($req['listId'] as $catId){
                $this->productsRepository->updateProductsFree($catId);

            }
        }else{
            $this->productCategoriesRepository->delete($req['id']);
            $this->productCategoriesRepository->deleteWhere(['parent' => $req['id']]);
            $this->productsRepository->updateProductsFree($req['id']);

        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate($req['parent'])));

    }

    public function create(Request $request) {
        $param = $request->all();

        // run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $this->productCategoriesRepository->rules());

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));

        }
        $exits = !empty($param['id'])? $this->productCategoriesRepository->find($param['id']): null;
        $level = 0;
        if($param['parent'] != 0){
            $level = 1;
        }
        if(!empty($param['cate_3'])){
            $level = 2;
        }
        try {
            $banner = !empty($param['banner'])? implode(',',$param['banner']) : "";

            $data = array(
                'name' => $param['name'], 
                'name_en' => $param['name_en'], 
                'slug' => str_slug($param['name']), 
                'images' => $param['images'],
                'description' => $param['description'], 
                'parent' => $param['parent'], 
                'orderBy' => $param['orderBy'], 
                'active' => $param['active'],
                'banner' => $banner,
                'thumbnail' => !empty($param['thumbnail'])? $param['thumbnail'] : '',
                'level' => $level

//                'created_at' => date('Y-m-d h:i:s'),
            );
            if (empty($exits)) {

                $this->productCategoriesRepository->create($data);
            } else {
                if($exits->parent != 0){
                    $this->productsRepository->updateProductByCategory($exits->id, $param['parent']);
                }

                $this->productCategoriesRepository->update($data, $param['id']);
            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate($param['parent'])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));
        }
    }
    public function getInit(Request $request){
        return $this->getNewUpdate($request->get('parent'));
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
                'rows' => $this->productCategoriesRepository
                    ->orderBy('id','desc')
                    ->findWhere(["parent" => $cond]),
                'rules' => $this->productCategoriesRepository->rules()
            )
        );
    }
}
