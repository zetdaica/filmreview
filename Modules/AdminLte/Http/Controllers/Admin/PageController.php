<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Repositories\PagesRepository;
use Modules\AdminLte\Repositories\PageCategoriesRepository;
use Modules\AdminLte\Entities\Pages;
use Modules\AdminLte\Http\Controllers\BaseController;
use Validator,Response;

class PageController extends BaseController
{
    public $pageRepo;
    public $pageCategoriesRepo;
    function __construct(PagesRepository $pageRepo, PageCategoriesRepository $pageCategoriesRepo){
        parent::lteInit();
        $this->pageRepo = $pageRepo;
        $this->pageCategoriesRepo = $pageCategoriesRepo;
        $this->data['routeSave'] = route('backend.page.save');
        $this->data['routeDelete'] = route('backend.page.delete');
        $this->data['routeInit'] = route('backend.page.init');
        $this->data['current'] = 'page';

    }
    public function pagesOfCat($catId){
        $this->data['title'] = 'Danh muc';
        $this->data['slug'] = '';
        $this->data['cat'] = $this->pageCategoriesRepo->find($catId);
        $this->data['categories'] = $this->pageCategoriesRepo
            ->findWhere([], ['id','name']);
        $this->data['viewContent'] = 'adminlte::page.module.list';
        return view('adminlte::page.index', $this->data);
    }

    public function detail($slug){
        $this->data['title'] = 'Slug:'. $slug;
        $this->data['slug'] = $slug;
        $this->data['detail'] = $this->pageRepo->findByField('slug', $slug);
        $this->data['cat'] = $this->pageCategoriesRepo->whereHas('pages', function ($query) use ($slug){
            return $query->where('slug',$slug);
        })->findWhere([])->first();

        $this->data['categories'] = $this->pageCategoriesRepo
            ->findWhere([], ['id','name']);
        $this->data['viewContent'] = 'adminlte::page.module.detail';
        return view('adminlte::page.index', $this->data);
    }

    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->pageRepo->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            Pages::whereIn('id', $req['listId'])->delete();

        }else{
            $this->pageRepo->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate(['FKCategory' => $req['catId']])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->pageRepo->rules(), $this->pageRepo->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->pageRepo->find($param['id']): null;

        try {
            $data = array(
                'FKCategory' => $param['FKCategory'],
                'name' => $param['name'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'thumbnail' => '',
                'description' => $param['description'],
                'content' => $param['content'],
                'active' => $param['active'],
                'orderBy' => $param['orderBy'],
            );
            if (empty($exits)) {
                $this->pageRepo->create($data);
            } else {
                $this->pageRepo->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate(['FKCategory' => $param['FKCategory']])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(Request $request){
        return $this->getNewUpdate(['FKCategory' => $request->get('catId')]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
                'rows' => $this->pageRepo
                    ->orderBy('id','desc')
                    ->findWhere($cond),
                'rules' => $this->pageRepo->rules()
            )
        );
    }
}
