<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\ParametersRepository;
use Validator,Response;
use Modules\AdminLte\Repositories\UsersFrontRepository;
use Modules\AdminLte\Entities\UsersFront;

class UsersFrontController extends BaseController
{
    protected $usersFrontRepository;
    protected $parametersRepository;
    function __construct(UsersFrontRepository $usersFrontRepository, ParametersRepository $parametersRepository){
        parent::lteInit();

        $this->usersFrontRepository = $usersFrontRepository;
        $this->parametersRepository = $parametersRepository;
        $this->data['routeSave'] = route('backend.user.front.save');
        $this->data['routeDelete'] = route('backend.user.front.delete');
        $this->data['routeInit'] = route('backend.user.front.init');

        $this->data['current'] = '';
    }
    public function index(){
        $this->data['title'] = 'Danh sách khách hàng đã đăng ký';
        $this->data['dataJson'] = $this->getNewUpdate([]);
        $this->data['listCustomerType'] = $this->parametersRepository->findWhere(["paramCode" => "CustomerType"]);
        $this->data['viewContent'] = 'adminlte::usersfront.module.list';
    	return view('adminlte::usersfront.index', $this->data);
    }

    public function edit(Request $request){
        return Response::json(array('success' => true, 'result' => $this->usersFrontRepository->find($request->get('id'))));

    }
    public function delete(Request $request){
        $req = $request->all();
        if(!empty($req['listId'])){
            UsersFront::whereIn('id', $req['listId'])->delete();

        }else{
            $this->usersFrontRepository->delete($req['id']);
        }
        return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

    }

    public function create(Request $request) {
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->usersFrontRepository->rules(), $this->usersFrontRepository->messages());

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->usersFrontRepository->find($param['id']): null;

        try {

            $data = array(
                'name' => $param['name'],
                'username' => $param['username'],
                'email' => $param['email'],
                'address' => $param['address'],
                'phone' => $param['phone'],
                'type' => $param['type'],

            );
            if (empty($exits)) {
                $this->usersFrontRepository->create($data);
            } else {
                $this->usersFrontRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true, 'result' => $this->getNewUpdate([])));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }
    public function getInit(){
        return $this->getNewUpdate([]);
    }
    protected function getNewUpdate($cond){
        return  json_encode(array(
            'rows' => $this->usersFrontRepository
                ->orderBy('id','desc')
                ->with('customer_type')
                ->findWhere($cond)
                ->toArray(),
            'rules' => $this->usersFrontRepository->rules()
            )
        );
    }
}
