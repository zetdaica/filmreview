<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\BannersRepository;
use Modules\AdminLte\Repositories\ProjectsRepository;
use Modules\AdminLte\Repositories\VideosRepository;
use Validator,Response;

class VideoController extends BaseController
{
    protected $videosRepository;

    function __construct(VideosRepository $videosRepository){
        parent::lteInit();
        $this->videosRepository = $videosRepository;
        $this->data['routeSave'] = route('backend.video.save');
        $this->data['routeIndex'] = route('backend.video.index');
        $this->data['routeAdd'] = route('backend.video.add');
        $this->data["title"] = "Videos";
    }

    public function index(){
        $this->data["rows"] =$this->videosRepository->all();
        $this->data['viewContent'] = 'adminlte::video.module.list';
        return view('adminlte::video.index', $this->data);
    }

    public function add(){
        $this->data['viewContent'] = 'adminlte::video.module.add';
        return view('adminlte::video.index', $this->data);
    }

    public function edit($id){
        $this->data["detail"] =$this->videosRepository->findWhere(array('id' => $id))->first();
        $this->data['viewContent'] = 'adminlte::video.module.edit';
        return view('adminlte::video.index', $this->data);
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->videosRepository->rules(), $this->videosRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->videosRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'ytb_url' => $param['ytb_url'],
                'slug' => str_slug($param['name']),
                'images' => $param['images'],
                'description' => $param['description'],
                'content' => $param['content'],
                'status' => $param['status'],
                'order' => !empty($param['order'])? $param['order'] : 0,
            );
            if (empty($exits)) {
                $this->videosRepository->create($data);
            } else {
                $this->videosRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
