<?php

namespace Modules\AdminLte\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\AdminLte\Http\Controllers\BaseController;
use Modules\AdminLte\Repositories\AboutUsRepository;
use Modules\AdminLte\Repositories\BannersRepository;
use Validator,Response;

class StaticPageController extends BaseController
{
    protected $aboutUsRepository;

    function __construct(AboutUsRepository $aboutUsRepository){
        parent::lteInit();
        $this->aboutUsRepository = $aboutUsRepository;
        $this->data['routeSave'] = route('backend.about_us.save');
        $this->data['routeEdit'] = route('backend.about_us.edit');

        $this->data["title"] = "about_us";
    }

    public function edit(){
        $this->data["detail"] =$this->aboutUsRepository->firstOrNew();
        $this->data['viewContent'] = 'adminlte::about_us.module.edit';
        return view('adminlte::about_us.index', $this->data);
    }

    public function store(Request $request){
        $param = $request->all();
        $validator = Validator::make($request->all(), $this->aboutUsRepository->rules(), $this->aboutUsRepository->messages());

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->messages()));
        }
        $exits = !empty($param['id'])? $this->aboutUsRepository->find($param['id']): null;

        try {
            $data = array(
                'name' => $param['name'],
                'top_text' => $param['top_text'],
                'top_image' => $param['top_image'],
                'middle_text' => $param['middle_text'],
                'middle_image' => $param['middle_image'],
            );

            if (empty($exits)) {
                $this->aboutUsRepository->create($data);
            } else {
                $this->aboutUsRepository->update($data, $param['id']);

            }
            return Response::json(array('success' => true));

        } catch (\Exception $e) {
            return Response::json(array('success' => false, 'message' => $e->getMessage()));

        }
    }


}
