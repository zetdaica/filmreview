<?php

namespace Modules\AdminLte\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Entities\ProductCategories;
use App\Entities\PageCategories;
use Modules\AdminLte\Entities\Options;
use Modules\AdminLte\Entities\Pages;
use Modules\AdminLte\Entities\Parameters;

class BaseController extends Controller
{
    public $data;
    const POST_TYPE = 'PostType';
    const ATTRIBUTE_TYPE = 'AttributeType';
    const CONFIG_TYPE = 'configType';

    const PARENT_POST = 0;
    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct()
    {
    }

    public function lteInit()
    {

        $this->data['title'] = "";
//        $this->data['catLev1'] = ProductCategories::where(["parent" => 0])->orderBy('parent', 'asc')->get();
//        $this->data['catLev2'] = ProductCategories::where(array(["parent", '<>', 0], "level" =>  1))->orderBy('parent', 'asc')->get();
//        $this->data['catLev3'] = ProductCategories::where("level",  2)->orderBy('parent', 'asc')->get();

//        $this->data['pageCates'] = PageCategories::get();

        $this->data['routeSave'] = '';
        $this->data['routeDelete'] = '';
        $this->data['routeGetEdit'] = '';
        $this->data['current'] = '';
        $this->middleware('auth');
//        $this->data['subjects'] = Pages::whereHas('getPageCategory', function ($query) {
//            $query->where('slug', 'bai-viet');
//        })->orderBy('orderBy', 'asc')->get();
        $this->data['options'] = Options::all();
        $this->data['configType'] = Parameters::getConfigType();

    }
    public function lteNonAuth(){
        $this->data['title'] = "";
        $this->data['catLev1'] = ProductCategories::where(["parent" => 0])->orderBy('parent', 'asc')->get();
        $this->data['catLev2'] = ProductCategories::where(array(["parent", '<>', 0], "level" =>  1))->orderBy('parent', 'asc')->get();
        $this->data['catLev3'] = ProductCategories::where("level",  2)->orderBy('parent', 'asc')->get();

        $this->data['pageCates'] = PageCategories::get();

        $this->data['routeSave'] = '';
        $this->data['routeDelete'] = '';
        $this->data['routeGetEdit'] = '';
        $this->data['current'] = '';
        $this->data['subjects'] = Pages::whereHas('getPageCategory', function ($query) {
            $query->where('slug', 'bai-viet');
        })->orderBy('orderBy', 'asc')->get();

    }

    public function responseResultSuccess($code, $array = null){
        $arrayResult = array(
            'code' => $code,
        );
        $arrayResult = !empty($array) ? array_merge($arrayResult, $array) : $arrayResult;
        return response()->json($arrayResult, 200);
    }
    public function responseResultFail($errorMsg){
        return response()->json(array('success' => false , 'message' => $errorMsg));
    }
    public function responseBasicResult($array){
        return response()->json($array);
    }
}
