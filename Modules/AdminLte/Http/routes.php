<?php

Route::group(['middleware' => 'web', 'prefix' => LaravelLocalization::setLocale().'/admin', 'namespace' => 'Modules\AdminLte\Http\Controllers\Admin'], function()
{

    Route::get('/', array('uses' => 'LoginController@showLogin'));

    Route::get('/login', array('uses' => 'LoginController@showLogin'))->name('backend.admin.login');

// route to process the form
    Route::post('/login', array('uses' => 'LoginController@doLogin'))->name('backend.login.post');
    Route::get('/logout', array('uses' => 'LoginController@doLogout'))->name('backend.admin.logout');

    Route::post('/log_in','UserController@postLogIn');
    Route::post('/sign_in','UserController@postSignUp');

    Route::group(['prefix'=>'category'],function(){
        Route::get('/','CategoryController@index')->name('backend.category.index');
        Route::get('/child/{id}','CategoryController@child')->name('backend.category.child');
        Route::get('/add/','CategoryController@add')->name('backend.catedory.product.add');
        Route::get('/add/{id}','CategoryController@addChild')->name('backend.catedory.product.add.child');

        Route::get('/edit','CategoryController@edit')->name('backend.category.get.edit');

        Route::post('/save','CategoryController@create')->name('backend.category.save');
        Route::post('/delete','CategoryController@delete')->name('backend.category.delete');
        Route::get('/get-init','CategoryController@getInit')->name('backend.category.init');
        Route::get('/child-3/{id}','CategoryController@level3')->name('backend.category.level3');

    });
    Route::group(['prefix'=>'product'],function(){
        Route::get('/by-category/{id}','ProductController@productsOfChild')->name('backend.products_of_child');
        Route::get('/','ProductController@index')->name('backend.product.index');
        Route::post('/save','ProductController@create')->name('backend.product.save');
        Route::post('/delete','ProductController@delete')->name('backend.product.delete');
        Route::get('/get-init','ProductController@getInit')->name('backend.product.init');

        Route::get('/deleted/','ProductController@productsDeleted')->name('backend.products_of_child_deleted');

        Route::get('/by-category-level3/{id}','ProductController@productsOfChildLevel3')->name('backend.products.child.level3');

    });
    Route::group(['prefix'=>'pageCategory'],function(){
        Route::get('/','PageCategoryController@index')->name('backend.page.category.index');
        Route::get('/add','PageCategoryController@add');
        Route::post('/save','PageCategoryController@create')->name('backend.page.category.save');
        Route::post('/delete','PageCategoryController@delete')->name('backend.page.category.delete');
//        Route::get('/page/{id}','PageCategoryController@categoryPage')->name("backend.page.category.child");
//        Route::get('/page-cat/{slug}','PageCategoryController@categoryPageDetail')->name("backend.page.category.detail");
        Route::get('/get-init','PageCategoryController@getInit')->name('backend.page.category.init');

    });
    Route::group(['prefix'=>'page'],function(){
        Route::get('/by-category/{id}','PageController@pagesOfCat')->name('backend.pages.of.cat');
        Route::get('/detail/{slug}','PageController@detail')->name('backend.page.detail');
        Route::post('/save','PageController@create')->name('backend.page.save');
        Route::post('/delete','PageController@delete')->name('backend.page.delete');

        Route::get('/get-init','PageController@getInit')->name('backend.page.init');

    });

    Route::group(['prefix'=>'post'],function(){
        Route::get('/','PostController@index')->name('backend.post.index');
        Route::get('/add/{id}','PostController@add')->name('backend.post.add');
        Route::get('/deleteOne/{id}','PostController@deleteOne')->name('backend.post.deleteOne');

        Route::get('/detail/{slug}','PostController@detail')->name('backend.post.detail');
        Route::post('/save','PostController@create')->name('backend.post.save');
        Route::post('/delete','PostController@delete')->name('backend.post.delete');
        Route::get('/get-init','PostController@getInit')->name('backend.post.init');
        Route::get('/get_post_by_page_type','PostController@getPostByPageType')->name('backend.post.get_post_by_page_type');

        Route::get('/get-by-page/{id}','PostController@postByPage')->name('backend.post.postByPage');

    });
    Route::group(['prefix'=>'parameter'],function(){
        Route::get('/','ParameterController@index')->name('backend.parameter.index');
        Route::get('/edit/{id}','ParameterController@edit')->name('backend.parameter.get.edit');
        Route::post('/save','ParameterController@create')->name('backend.parameter.save');
        Route::post('/delete','ParameterController@delete')->name('backend.parameter.delete');
        Route::get('/get-init','ParameterController@getInit')->name('backend.parameter.init');

    });
    Route::group(['prefix'=>'option'],function(){
        Route::get('/','OptionController@index')->name('backend.option.index');
        Route::get('detail/{id}','OptionController@detail')->name('backend.option.detail');

        Route::post('/save','OptionController@create')->name('backend.option.save');
        Route::post('/delete','OptionController@delete')->name('backend.option.delete');
        Route::get('/get-init','OptionController@getInit')->name('backend.option.init');

    });
    Route::group(['prefix'=>'order'],function(){
        Route::get('/','OrderController@index')->name('backend.order.index');
        Route::post('/save','OrderController@create')->name('backend.order.save');
        Route::post('/delete','OrderController@delete')->name('backend.order.delete');
        Route::get('/get-init','OrderController@getInit')->name('backend.order.init');
        Route::post('/update','OrderController@update')->name('backend.user.front.update');

        Route::get('/detail/{id}','OrderController@detail')->name('backend.order.admin.detail');

    });
    Route::group(['prefix'=>'user-front'],function(){
        Route::get('/','UsersFrontController@index')->name('backend.user.front.index');
        Route::post('/save','UsersFrontController@create')->name('backend.user.front.save');
        Route::post('/delete','UsersFrontController@delete')->name('backend.user.front.delete');
        Route::get('/get-init','UsersFrontController@getInit')->name('backend.user.front.init');

    });
    Route::group(['prefix'=>'viet'],function(){
        Route::get('/','AdminLteController@index')->name('backend.vietlot.index');
        Route::post('/save','AdminLteController@create')->name('backend.vietlot.save');
        Route::post('/delete','AdminLteController@delete')->name('backend.vietlot.delete');
        Route::get('/get-init','AdminLteController@getInit')->name('backend.vietlot.init');
        Route::get('/get-random','AdminLteController@randomToVietlot')->name('backend.vietlot.random');

    });


    Route::group(['prefix'=>'banner'],function(){
        Route::get('/','BannerController@index')->name('backend.banner.index');
        Route::get('/add','BannerController@add')->name('backend.banner.add');
        Route::get('/edit/{id}','BannerController@edit')->name('backend.banner.edit');
        Route::get('/detail/{slug}','BannerController@detail')->name('backend.banner.detail');
        Route::post('/save','BannerController@store')->name('backend.banner.save');
        Route::get('/delete/{id}','BannerController@delete')->name('backend.banner.delete');
    });
    Route::group(['prefix'=>'member'],function(){
        Route::get('/','MemberController@index')->name('backend.member.index');
        Route::get('/add','MemberController@add')->name('backend.member.add');
        Route::get('/edit/{id}','MemberController@edit')->name('backend.member.edit');
        Route::get('/detail/{slug}','MemberController@detail')->name('backend.member.detail');
        Route::post('/save','MemberController@store')->name('backend.member.save');
        Route::get('/delete/{id}','MemberController@delete')->name('backend.member.delete');
    });
    Route::group(['prefix'=>'project'],function(){
        Route::get('/','ProjectController@index')->name('backend.project.index');
        Route::get('/add','ProjectController@add')->name('backend.project.add');
        Route::get('/edit/{id}','ProjectController@edit')->name('backend.project.edit');
        Route::get('/detail/{slug}','ProjectController@detail')->name('backend.project.detail');
        Route::post('/save','ProjectController@store')->name('backend.project.save');
        Route::get('/delete/{id}','ProjectController@delete')->name('backend.project.delete');
    });
    Route::group(['prefix'=>'album'],function(){

        Route::get('/{id}','AlbumController@album')->name('backend.album.index');
        Route::get('/add/{id}','AlbumController@add')->name('backend.album.add');
        Route::get('/edit/{projectId}/{id}','AlbumController@edit')->name('backend.album.edit');
        Route::get('/detail/{slug}','AlbumController@detail')->name('backend.album.detail');
        Route::post('/save','AlbumController@store')->name('backend.album.save');
        Route::get('/delete/{id}','AlbumController@delete')->name('backend.album.delete');
    });

    Route::group(['prefix'=>'news'],function(){

        Route::get('/{id}','NewsController@index')->name('backend.news.index');
        Route::get('/add/{id}','NewsController@add')->name('backend.news.add');
        Route::get('/edit/{catId}/{id}','NewsController@edit')->name('backend.news.edit');
        Route::get('/detail/{slug}','NewsController@detail')->name('backend.news.detail');
        Route::post('/save','NewsController@store')->name('backend.news.save');
        Route::get('/delete/{catId}/{id}','NewsController@delete')->name('backend.news.delete');
    });

    Route::group(['prefix'=>'news_category'],function(){
        Route::get('/','NewsCategoryController@index')->name('backend.news_category.index');
        Route::get('/add','NewsCategoryController@add')->name('backend.news_category.add');
        Route::get('/edit/{id}','NewsCategoryController@edit')->name('backend.news_category.edit');
        Route::get('/detail/{slug}','NewsCategoryController@detail')->name('backend.news_category.detail');
        Route::post('/save','NewsCategoryController@store')->name('backend.news_category.save');
        Route::get('/delete/{id}','NewsCategoryController@delete')->name('backend.news_category.delete');
    });
    Route::group(['prefix'=>'about_us'],function(){
        Route::get('/edit','StaticPageController@edit')->name('backend.about_us.edit');
        Route::post('/save','StaticPageController@store')->name('backend.about_us.save');
    });
    Route::group(['prefix'=>'system_feature'],function(){
        Route::get('/','SystemFeatureController@index')->name('backend.system_feature.index');
        Route::get('/add','SystemFeatureController@add')->name('backend.system_feature.add');
        Route::get('/edit/{id}','SystemFeatureController@edit')->name('backend.system_feature.edit');
        Route::get('/detail/{slug}','SystemFeatureController@detail')->name('backend.system_feature.detail');
        Route::post('/save','SystemFeatureController@store')->name('backend.system_feature.save');
        Route::get('/delete/{id}','SystemFeatureController@delete')->name('backend.system_feature.delete');

        Route::get('/oneFeature/{key}','SystemFeatureController@oneFeature')->name('backend.system_feature.oneFeature');

    });

    Route::group(['prefix'=>'video'],function(){
        Route::get('/','VideoController@index')->name('backend.video.index');
        Route::get('/add','VideoController@add')->name('backend.video.add');
        Route::get('/edit/{id}','VideoController@edit')->name('backend.video.edit');
        Route::get('/detail/{slug}','VideoController@detail')->name('backend.video.detail');
        Route::post('/save','VideoController@store')->name('backend.video.save');
        Route::get('/delete/{id}','VideoController@delete')->name('backend.video.delete');
    });
});
