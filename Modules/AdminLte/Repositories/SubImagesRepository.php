<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Albums;
use Modules\AdminLte\Entities\SubImages;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class SubImagesRepository extends BaseRepository
{
    public function model()
    {
        return SubImages::class;
    }
    public function rules(){
        return array(
//            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
//            'name.required' => 'Chưa nhập tên',
        ];
    }

    public function insertMultiRows($data){
        return $this->model->insert($data);
    }



}
