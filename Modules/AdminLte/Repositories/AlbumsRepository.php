<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Albums;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class AlbumsRepository extends BaseRepository
{
    public function model()
    {
        return Albums::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

    public function getRandomAlbums(){
        return $this->model->select(['albums.*', 'projects.slug as project_slug', 'projects.name as project_name'])
            ->join('projects', 'projects.id', '=' , 'albums.project_id', 'left')
            ->where(array())->orderBy('order', 'asc')->get();
    }

}
