<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Videos;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class VideosRepository extends BaseRepository
{
    public function model()
    {
        return Videos::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

}
