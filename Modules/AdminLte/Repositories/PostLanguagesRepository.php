<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\PostLanguages;
use Modules\AdminLte\Entities\Posts;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class PostLanguagesRepository extends BaseRepository
{
    public function model()
    {
        return PostLanguages::class;
    }


    public function deleteMultiPosts(array $list)
    {
        return $this->model->whereIn('post_id', $list)->delete();
    }
    public function createOrganizationLanguage($data, $postId){
        foreach ($data as $langs){
            $langs['post_id'] =  $postId;
            $check = $this->findWhere(array('post_id' => $postId, 'lang_code' => $langs['lang_code']))->first();
            if(empty($check)){
                $this->create($langs);

            }else{
                $this->updateWhere($langs, array('post_id' => $postId, 'lang_code' => $langs['lang_code']));
            }
        }
        return true;
    }
    public function updateWhere($arr, $cond){
        $this->model->where($cond)->update($arr);
    }

}
