<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\News;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class NewsRepository extends BaseRepository
{
    public function model()
    {
        return News::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên',
        ];
    }

    public function getWithLimit($asc, $limit = 10){
        $orderBy = $asc? 'asc' : 'desc';
        return $this->model->where('status', 1)->orderBy('id', $orderBy)->limit($limit)->get();
    }

    public function getNewsByAtribute($attr, $asc, $limit = 10){
        $orderBy = $asc? 'asc' : 'desc';

        return $this->model->where([array("attribute","like","%".$attr."%")])
            ->limit($limit)
            ->orderBy('id', $orderBy)
            ->get();
    }

}
