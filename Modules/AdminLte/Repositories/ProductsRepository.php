<?php

namespace Modules\AdminLte\Repositories;

use Modules\AdminLte\Entities\Products;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class ProductsRepository extends BaseRepository
{
    public function model()
    {
        return Products::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên sản phẩm',
        ];
    }
    public function selectNoField($fields){
        $arr = [
            'id',
            'FKCategory',
            'name',
            'slug',
            'images',
            'thumbnail',
            'type',
            'description',
            'content',
            'price',
            'active',
            'orderBy',
            'parentID',
            'created_at',
            'updated_at',
            'tags',
            'sales',
        ];
        if(!is_null($fields)){
            return array_diff($arr,$fields);
        }
        return $arr;
    }
    public function updateProductsFree($id){
        return $this->model->where('FKCategory', $id)->update(['FKCategory' => 0, 'parentID' => 0]);
    }
    public function updateProductByCategory($catId, $new){
        return $this->model->where('FKCategory', $catId)->update(['parentID' => $new]);
    }
    public function getViewd(){
        $cache = [];
        if (session()->has('viewed')) {
            $cache = session()->get('viewed');
        }
        return $this->scopeQuery(function ($q){
            return $q->where('active', 1);
        })->findWhereIn('id', $cache);

    }

    public function getProductsWithLimit($cond, $limit = 12){
        return $this->model->where($cond)->limit($limit)->orderBy('id', 'desc')->get();
    }
}
