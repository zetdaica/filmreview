<?php

namespace Modules\AdminLte\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Modules\AdminLte\Entities\ProductCategories;

/**
 * Interface ProductCategoriesRepository
 * @package namespace App\Repositories;
 */
class ProductCategoriesRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductCategories::class;
    }
    public function selectNoField($fields){
        $arr =  [
            'id', 'name', 'slug', 'images', 'active', 'orderBy', 'created_at', 'updated_at', 'description', 'parent', 'banner', 'thumbnail'
        ];
        if(!is_null($fields)){
            return array_diff($arr,$fields);
        }
        return $arr;
    }
    public function rules(){
        return array(
            'name'    => 'required',
        );
    }
    public function updateCategoriesLevel2($old, $new){
        return $this->model->where('parent',$old)->update(['parent' => $new]);
    }

}
