<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Products extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'products';
    public $fillable = [
        'id',
        'FKCategory',
        'name',
        'slug',
        'images',
        'thumbnail',
        'type',
        'description',
        'content',
        'price',
        'active',
        'orderBy',
        'parentID',
        'created_at',
        'updated_at',
        'tags',
        'sales',
    ];
    public function getParameter(){
        return $this->hasOne(Parameters::class, 'paramID', 'type');
    }

}
