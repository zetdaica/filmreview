<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Pages extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'pages';

    protected $fillable = [
        'id', 'name', 'FKCategory', 'slug', 'images','description', 'content', 'thumbnail', 'orderBy', 'active', 'created_at', 'updated_at',
    ];

}
