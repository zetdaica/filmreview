<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Options extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id', 'optionID','value', 'name', 'active', 'created_at', 'updated_at',
    ];
    protected $table = "options";

}
