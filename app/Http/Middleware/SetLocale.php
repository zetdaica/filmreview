<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Session;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $locale = $request->segment(1);
//        $seg2 = $request->segment(2);
//        if($locale == 'admin' || $seg2 == 'admin'){
//            return $next($request);
//        }
//        if ( ! array_key_exists($locale, Config::get('app.locales'))) {
//            $segments = $request->segments();
//            $segments[0] = Config::get('app.fallback_locale');
//
//            return Redirect::to(implode('/', $segments));
//        }
//
//        App::setLocale($locale);

        return $next($request);
    }
}
