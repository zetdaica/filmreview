<?php

namespace App\Http\Controllers;

use App\Entities\Options;

class BaseController extends Controller
{

    public $data;
    const HOME_PAGE = 1;
    const ACTIVE = 1;
    const INATIVE = 0;
    public function init(){

        $this->data['postActive'] = '';
        $this->data['menuActive'] = '';
        $options = Options::where('active',1)->get();
        $opt = array();
        foreach ($options as $k => $v) {
            $opt[$v->optionID] = $v->value;
        }
        $this->data['options'] = $opt;
        $this->data['pageTitle'] = !empty($opt['page_title'])? $opt['page_title'] : 'PHIM TRƯỜNG ALIBABA ';

    }
}
