<?php

namespace App\Http\Controllers\Front;


use App\Http\Requests;

use App\Models\Pages;
use App\Http\Controllers\BaseController;

use App\Repositories\ProductCategoriesRepository;
use Modules\AdminLte\Repositories\ProductsRepository;

class HomeController extends BaseController
{

    protected $productsRepository;
    protected $productCategoriesRepository;

    function __construct(ProductsRepository $productsRepository , ProductCategoriesRepository $productCategoriesRepository){
        parent::init();
        $this->productsRepository = $productsRepository;
        $this->productCategoriesRepository = $productCategoriesRepository;
    }


    public function index(){

		$this->data['viewContent'] = 'front.home.module.content';
    	return view('front.home.index', $this->data);
    }


}
