<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Parameters;

class ParametersRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Parameters::class;
    }
    public function rules(){
        return array(
            'paramID'    => 'required',
            'value'    => 'required',
            'paramCode'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'paramID.required' => 'Chưa nhập ParamID',
            'value.required' => 'Chưa nhập giá trị',
            'paramCode.required' => 'Chưa nhập Param Code',
        ];
    }

}