<?php

namespace App\Repositories;

use App\Entities\Pages;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ProductsRepository
 * @package namespace App\Repositories;
 */
class PagesRepository extends BaseRepository
{
    public function model()
    {
        return Pages::class;
    }
    public function rules(){
        return array(
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'name.required' => 'Chưa nhập tên sản phẩm',
        ];
    }

}
