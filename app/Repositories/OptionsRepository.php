<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Options;

class OptionsRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Options::class;
    }
    public function rules(){
        return array(
            'optionID'    => 'required',
            'value'    => 'required',
            'name'    => 'required',

        );
    }

    public function messages()
    {
        return [
            'optionID.required' => 'Chưa nhập Option ID',
            'value.required' => 'Chưa nhập giá trị',
            'name.required' => 'Chưa nhập tên',
        ];
    }

}