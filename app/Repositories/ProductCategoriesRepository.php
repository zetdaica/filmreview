<?php

namespace App\Repositories;

use App\Models\CoreModel;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\ProductCategories;

/**
 * Interface ProductCategoriesRepository
 * @package namespace App\Repositories;
 */
class ProductCategoriesRepository extends CoreModel
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductCategories::class;
    }
    public function selectNoField($fields){
        $arr =  [
            'id', 'name', 'slug', 'images', 'active', 'orderBy', 'created_at', 'updated_at', 'description', 'parent', 'banner'
        ];
        if(!is_null($fields)){
            return array_diff($arr,$fields);
        }
        return $arr;
    }
    public function rules(){
        return array(
            'name'    => 'required',
        );
    }

}
