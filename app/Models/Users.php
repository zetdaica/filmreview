<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = "users";
    
    public static function CreateUser($data) {
    	try {
            $check = self::where('username', $data['username'])->first();
	        if(count($check) == 0) {
	            $user = new self;
	            $user->username = $data['username'];
	            $user->password = bcrypt($data['password']);
	            $user->name = $data['name'];
	            $user->save();
	        }
            return array('success' => true);
        } catch (Exception $e) {
            return array('success' => false, 'message' => $e);
        }
    }
}
