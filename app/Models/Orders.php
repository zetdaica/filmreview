<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public static function newOrder(){
        $row = new self;
        $row->name = "";
        $row->value = "";
        $row->active = 1;
        $row->created_at = "";
        $row->updated_at = "";
        return $row;
    }
    public static function DeleteByID($listID){
        if(!empty($listID)){
            try{
                foreach ($listID as $v) {
                    Orders::where('id', $v)->delete();
                }
                return array('success' => true,"message"=> $listID);
            }catch(Exception $e){
                return array('success' => false, 'message' => $e);
            }

        }
    }
}
