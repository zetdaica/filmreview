<?php

namespace App\Models;

use Prettus\Repository\Eloquent\BaseRepository;

class CoreModel extends BaseRepository
{

    public function model()
    {
        // TODO: Implement model() method.
        return $this->model;
    }

    public function selectWhere($cond, $order){

        return $this->model->where($cond)->orderBy($order[0], $order[1])->get();
    }
}
