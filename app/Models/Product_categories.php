<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_categories extends Model
{
   public static function newCat(){
	            $row = new self;
	            $row->name = "";
	            $row->name_en = "";
	            $row->slug = "";
	            $row->images = "";
	            $row->description = "";
	            $row->parent = 0;
	            $row->active = 1;
	            $row->orderBy = 0;
                $row->banner = '';
                $row->created_at = "";
	            $row->updated_at = "";
            return $row;
   }
   public function products(){
       return $this->hasMany(Products::class,"FKCategory","id");
   }
   public function getBanner(){
       return $this->hasOne(Pages::class, "id", "banner");
   }
   public function _child_three(){
       return $this->hasMany(Product_categories::class, "parent", "id");
   }
   public static function DeleteByID($listID){
   		if(!empty($listID)){
   			try{
   				foreach ($listID as $v) {
	   				Product_categories::where('id', $v)->delete();
	   			}
            	return array('success' => true,"message"=> $listID);
   			}catch(Exception $e){
            	return array('success' => false, 'message' => $e);
   			}
   			
   		}
   }
   public static function getAllFloor(){

      $level1 = Product_categories::where('parent',0)->orderBy('orderBy', 'asc')->get();
       $arr = array('dochoi','dososinh','bengu','dinhduongbome','giaydep','binhsua','behoc','xechobe','quanao','chome','bimta','betam','anuong','antoan');

       if(!empty($level1)){
         foreach ($level1 as $key => $value) {
             $c = false;
             $level2 = Product_categories::where('parent',$value->id)->with('_child_three')->orderBy('orderBy', 'asc')->get();
             $level1[$key]->banners = Pages::whereIn('id', explode(',',$value->banner))->get();
             $level1[$key]->products = array();
             $level1[$key]->products_banner = array();
             $level1[$key]->icon = $arr[$key];
             foreach ($level2 as $k => $v) {
                $level2[$k]->p = array();
//                $p = Products::where('FKCategory',$v->id)->limit(8)->get();
                 $b = Products::where(['FKCategory' => $v->id, 'type' => 'PT02', 'active' => 1])->limit(8)->get();
//                 $level2[$k]->products = $p;

                 $level2[$k]->products = $b;
             }
             $products = Products::where(['parentID' => $value->id, 'type' => 'PT02', 'active' => 1])->inRandomOrder()->limit(2)->get();
             $productsRight = Products::where(['parentID' => $value->id, 'type' => 'PT04', 'active' => 1])->inRandomOrder()->limit(8)->get();
            if(count($products) > 0 || count($productsRight) > 0){
                  $level1[$key]->products = $products;
                  $c = true;
            }
             $level1[$key]->right_products = $productsRight;

             $level1[$key]->level2 = $level2;
            $level1[$key]->flag = $c;
         }
      }
      return $level1;
    }
}
