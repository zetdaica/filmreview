<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page_categories extends Model
{
    public static function newCat(){
                $row = new self;
                $row->name = "";
                $row->name_en = "";
                $row->slug = "";
                $row->images = "";
                $row->description = "";
                $row->parent = 0;
                $row->active = 1;
                $row->orderBy = 0;
                $row->created_at = "";
                $row->updated_at = "";
            return $row;
   }
   public static function DeleteByID($listID){
        if(!empty($listID)){
            try{
                foreach ($listID as $v) {
                    Page_categories::where('id', $v)->delete();
                }
                return array('success' => true,"message"=> $listID);
            }catch(Exception $e){
                return array('success' => false, 'message' => $e);
            }
            
        }
   }
}
