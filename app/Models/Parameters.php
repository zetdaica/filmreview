<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parameters extends Model
{
   public static function newParameter(){
	            $row = new self;
	            $row->paramID = "";
	            $row->value = "";
	            $row->active = 1;
	            $row->created_at = "";
	            $row->updated_at = "";
            return $row;
   }
   public static function DeleteByID($listID){
   		if(!empty($listID)){
   			try{
   				foreach ($listID as $v) {
	   				Product_categories::where('id', $v)->delete();
	   			}
            	return array('success' => true,"message"=> $listID);
   			}catch(Exception $e){
            	return array('success' => false, 'message' => $e);
   			}
   			
   		}
   }
}
