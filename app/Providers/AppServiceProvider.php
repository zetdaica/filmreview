<?php

namespace App\Providers;

use App\FormHelpers;
use Illuminate\Support\ServiceProvider;
use Prettus\Repository\Providers\RepositoryServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        parent::boot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
